<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePenerbitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql')->create('penerbits', function (Blueprint $table) {
            $table->id();
            $table->string('owner');
            $table->string('no_telp');
            $table->string('email');
            $table->string('nama_pt');
            $table->string('nama_brand');
            $table->string('bidang_usaha');
            $table->string('total_pendanaan');
            $table->string('deskripsi');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penerbits');
    }
}
