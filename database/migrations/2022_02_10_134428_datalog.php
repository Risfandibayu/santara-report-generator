<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Datalog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('datalogs', function (Blueprint $table) {
            $table->id();
            $table->string('owner');
            $table->string('no_telp');
            $table->string('email');
            $table->string('nama_pt');
            $table->string('nama_brand');
            $table->string('bidang_usaha');
            $table->string('total_pendanaan');
            $table->string('deskripsi');
            $table->string('status');
            $table->string('eid');
            $table->string('tid');
            $table->string('uid');
            $table->string('cid');
            $table->string('ket');
            $table->timestamp('timelog');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
