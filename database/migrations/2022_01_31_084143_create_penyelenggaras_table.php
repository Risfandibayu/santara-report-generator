<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePenyelenggarasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql')->create('penyelenggaras', function (Blueprint $table) {
            $table->id();
            $table->string('nama_pt');
            $table->string('nama_brand');
            $table->string('jumlah_penerbit');
            $table->string('jumlah_penerbit_funded');
            $table->string('jumlah_investor');
            $table->string('jumlah_investor_dividen');
            $table->string('jumlah_dana');
            $table->string('jumlah_dividen');
            $table->string('jumlah_dividen_penerima');
            $table->string('jenis');
            $table->string('jenis_jumlah');
            $table->string('lksm_negara');
            $table->string('lksm_prov');
            $table->string('lksm_jumlah');
            $table->string('lkst_negara');
            $table->string('lkst_prov');
            $table->string('lkst_jumlah');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penyelenggaras');
    }
}
