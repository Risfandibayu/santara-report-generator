<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePenerbitKodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    // protected $connection = 'mysql';
    
    public function up()
    {
        Schema::connection('mysql')->create('penerbit_kodes', function (Blueprint $table) {
            $table->id();
            $table->string('eid');
            $table->string('kode');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql')->dropIfExists('penerbit_kodes');
    }
}
