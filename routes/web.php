<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\DB;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::get('/test', function () {
    $em = db::table('emitens')->take(10)->get();
    if($em){
        echo "Connection success";
    }else{
        echo "Connection Failed";
    }
});
// Auth::routes();
Auth::routes(['verify' => true]);

Route::group(['middleware' => 'auth'], function() {
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::get('/penerbit', [App\Http\Controllers\PenerbitController::class, 'index']);
        Route::get('/penerbit/{id}', [App\Http\Controllers\PenerbitController::class, 'investor']);
    Route::get('/pnb', [App\Http\Controllers\PenerbitController::class, 'indexp']);
    Route::post('/penerbit/store', [App\Http\Controllers\PenerbitController::class, 'store']);
    Route::post('/penerbit/update/{id}', [App\Http\Controllers\PenerbitController::class, 'update']);
    Route::post('/penerbit/update_status/{id}', [App\Http\Controllers\PenerbitController::class, 'update_status']);
    
    Route::get('/cron_penerbit', [App\Http\Controllers\PenerbitController::class, 'cron_penerbit']);

    Route::post('/set_akun', [App\Http\Controllers\akunController::class, 'set_akun']);
    Route::post('/refresh_token', [App\Http\Controllers\akunController::class, 'refresh_token']);

    Route::get('/cron_refresh_token', [App\Http\Controllers\akunController::class, 'cron_refresh_token']);

    Route::get('/penyelenggara', [App\Http\Controllers\PenyelenggaraController::class, 'index']);
    Route::post('/penyelenggara/store', [App\Http\Controllers\PenyelenggaraController::class, 'store'])->name('penyelenggara.store');

    Route::get('/cron_penyelenggara', [App\Http\Controllers\PenyelenggaraController::class, 'cron_penyelenggara']);

    Route::get('/datalog', [App\Http\Controllers\datalogController::class, 'index'])->name('datalog');
    Route::post('/datalog_penerbit/{tgl}', [App\Http\Controllers\datalogController::class, 'detail_tgl'])->name('datalog_detail_tgl');
    Route::post('/datalog_penyelenggara/{tgl}', [App\Http\Controllers\datalogController::class, 'detailpny_tgl'])->name('datalog_detail_tglpny');
});