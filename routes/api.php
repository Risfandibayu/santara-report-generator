<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/cron_refresh_token', [App\Http\Controllers\akunController::class, 'cron_refresh_token']);
Route::get('/cron_penyelenggara', [App\Http\Controllers\PenyelenggaraController::class, 'cron_penyelenggara']);
Route::get('/cron_penerbit', [App\Http\Controllers\PenerbitController::class, 'cron_penerbit']);
