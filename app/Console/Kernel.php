<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();
        // $schedule->call('')->everyMinute();

        $schedule->call(function () {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "http://localhost/santara/api/cron_refresh_token");
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_exec($ch);
            curl_close($ch);
        })->cron('* * * * *');
        $schedule->call(function () {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "http://localhost/santara/api/cron_penerbit");
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_exec($ch);
            curl_close($ch);
        })->cron('0 3  * * 2,5');
        // $schedule->call(function () {
        //     $ch = curl_init();
        //     curl_setopt($ch, CURLOPT_URL, "http://localhost/santara/api/cron_refresh_token");
        //     curl_setopt($ch, CURLOPT_HEADER, 0);
        //     curl_exec($ch);
        //     curl_close($ch);
        // })->cron('0 3  * * 2,5');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
