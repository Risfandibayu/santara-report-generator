<?php

namespace App\Http\Controllers;

use App\Models\jenis;
use App\Models\penyelenggara;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class datalogController extends Controller
{
    public function index(){
        $pnblog = db::connection('mysql')->table('dtlogs')
        ->select(db::raw("date(timelog) as time"))
        ->groupBy(db::raw("date(timelog)"))
        ->orderBy('time','DESC')
        ->get();

        $pnylog = penyelenggara::select(db::raw("date(created_at) as time"))
        ->groupBy(db::raw("date(created_at)"))
        ->orderBy(db::raw("date(created_at)"),'DESC')
        ->get();

        return view('datalog.index',compact('pnblog','pnylog'));
    }
    public function detail_tgl($tgl){
        $pnbdet = db::connection('mysql')->table('dtlogs')
        ->select('dtlogs.*','dtlogs.created_at as create','statuses.status as sta')
        ->join('statuses','statuses.kode','=','dtlogs.status')
        ->where(db::raw("date(timelog)"),$tgl)
        ->orderBy('dtlogs.id', 'DESC')
        ->get();

        return view('datalog.detail_tglpnb',compact('pnbdet'));
    }
    public function detailpny_tgl($tgl){
        $pny = penyelenggara::select('*',db::raw("created_at as time"))
        
        ->where(db::raw("date(created_at)"),$tgl)
        ->get();

        $jenis = db::connection('mysql')->table('jenis_sbt')->select('*')->get();

        return view('datalog.detail_tglpny',compact('pny','jenis'));
    }
}
