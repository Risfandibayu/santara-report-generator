<?php

namespace App\Http\Controllers;

use App\Models\penyelenggara;
use App\Http\Requests\StorepenyelenggaraRequest;
use App\Http\Requests\UpdatepenyelenggaraRequest;
use App\Models\akun;
use App\Models\jenis;
use App\Models\prov_sbt;
use App\Models\provp_sbt;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;

class PenyelenggaraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $jumlah_penerbit = db::connection('mysql2')->table('emitens as e')->select("e.id")
        ->join('emiten_journeys as ej','ej.emiten_id','=','e.id')
        ->whereraw('e.is_deleted = 0 and e.is_active=1')
        ->groupBy('e.id')
        ->get();
        $jumlah_penerbit_funded = db::connection('mysql2')->table('emitens as e')->select("e.id")
        ->join('emiten_journeys as ej','ej.emiten_id','=','e.id')
        ->whereraw("e.is_deleted = 0 and e.is_active=1 and ej.title = 'Pendanaan Terpenuhi'")
        ->groupBy('e.id')
        ->get();

        // SELECT COUNT(ers.email) total_pemodal_total 
        // FROM users ers
        // JOIN traders tra on tra.user_id = ers.id
        // WHERE EXISTS (SELECT * FROM transactions ons 
        // WHERE ons.trader_id = tra.id AND ons.is_verified = 1 AND ons.is_deleted = 0 )

        $jumlah_investor = db::connection('mysql2')->table('users as ers')->select(db::raw("COUNT(ers.email) as total_pemodal_total"))
        ->join('traders as tra', 'tra.user_id','=','ers.id')
        ->whereRaw("EXISTS (SELECT * FROM transactions ons 
        WHERE ons.trader_id = tra.id AND ons.is_verified = 1 AND ons.is_deleted = 0 )")->first();

        // SELECT sum(b.devidend) from bagihasils as b
        // INNER JOIN traders as t on t.id = b.trader_id
        // WHERE EXISTS (SELECT * FROM transactions ons 
        // WHERE ons.trader_id = t.id AND ons.is_verified = 1 AND ons.is_deleted = 0 )

        $jumlah_investor_deviden = db::connection('mysql2')->table('bagihasils as b')
        ->select(db::raw('sum(b.devidend) as jid'))
        ->join('traders as t','t.id' ,'=','b.trader_id')
        ->whereraw('EXISTS (SELECT * FROM transactions ons WHERE ons.trader_id = t.id AND ons.is_verified = 1 AND ons.is_deleted = 0 )')
        ->first();

        $jumlah_deviden_penerima = db::connection('mysql2')->table('bagihasils as b')
        ->select(db::raw('sum(b.devidend) as jid'))
        ->join('traders as t','t.id' ,'=','b.trader_id')
        ->whereraw('EXISTS (SELECT * FROM transactions ons WHERE ons.trader_id = t.id AND ons.is_verified = 1 AND ons.is_deleted = 0 )')
        ->first();

//         SELECT SUM(amount) total_pendanaan
// FROM transactions 
// WHERE is_verified = 1 AND is_deleted = 0

        $jumlah_dana = db::connection('mysql2')->table('transactions as t')->select(db::raw('SUM(amount) as total_pendanaan'))->whereRaw('is_verified = 1 AND is_deleted = 0')->first();

//         SELECT sum(d.devidend) from devidend as d
// INNER JOIN emitens as e on e.id= d.emiten_id
        $jumlah_deviden = db::connection('mysql2')->table('devidend as d')->select(db::raw('sum(d.devidend) as d'))
        ->join('emitens as e','e.id','=','d.emiten_id')->first();

//         SELECT c.category,count(e.id) from emitens as e
// join categories as c on c.id = e.category_id
// inner join emiten_journeys as ej on ej.emiten_id=e.id
// WHERE e.is_deleted = 0 and e.is_active=1 and ej.title = 'Pendanaan Terpenuhi'
// GROUP BY category_id;

        $jenis = db::table('santarax_db.emitens as em')
        ->select('c.category as cat',db::raw('count(em.id) as jcat'),'c.id as cid')
        ->join('santarax_db.emiten_journeys as ej','ej.emiten_id','=','em.id')
        ->join('santarax_db.categories as c','em.category_id','=','c.id')
        // ->join('project_santara.jenis as jen','c.id','=','jen.kode')
        ->whereRaw("em.is_deleted = 0 and em.is_active=1 and ej.title = 'Pendanaan Terpenuhi'")
        ->groupBy('em.category_id')->get();

        // select tra.province, count(tra.province) from traders tra
        // join transactions ons on ons.trader_id = ons.id
        // where tra.is_verified = 1 and tra.is_deleted = 0 and tra.tnc = 1 and ons.is_deleted = 0 and ons.is_verified = 1
        // group by province;

        // $jen = db::table('project_santara.jenis as je')
        // ->select('je.kode','je,kode_ald')
        // ->where('je.kode',12)->get();

        $jen = db::connection('mysql')->table('jenis')
                                    ->select('*')
                                    ->get();

        // $d = array();
        // foreach($id as $i){
        // $d[] = $i->eid;
        // };

        $lok = db::connection('mysql')->table('prov')->select('*')->get();
        $lokkota = db::connection('mysql')->table('kotas')->select('*')->get();

        foreach($lok as $i){
            $d[] = $i->prov;
            };
        $lokasi_pemodal = db::connection('mysql2')->table('traders as tra')
        ->select('tra.province as prov', db::raw('count(tra.province) as jum'))
        ->join('transactions as ons','ons.trader_id' ,'=', 'ons.id')
        ->whereRaw("tra.is_verified = 1 and tra.is_deleted = 0 and tra.tnc = 1 and ons.is_deleted = 0 and ons.is_verified = 1")
        ->groupBy("province")
        ->whereIn('tra.province',$d)
        ->get();

        // SELECT p.name,count(e.id) from emitens as e
        // join regencies as r on e.regency_id=r.id
        // join provinces as p on p.id=r.province_id
        // WHERE e.is_deleted = 0 and e.is_active=1
        // GROUP BY r.province_id;

        $lokasi_penerbit = db::connection('mysql2')->table('emitens as e')
        ->select('p.name as prov','r.name as reg','p.id as pid','r.id as rid',db::raw('count(e.id) as jum'))
        ->join('regencies as r','e.regency_id','=','r.id')
        ->join('provinces as p','p.id','=','r.province_id')
        ->whereRaw('e.is_deleted = 0 and e.is_active=1')
        ->groupBy('r.id')
        ->get();

        return view('penyelenggara.index',compact('jumlah_penerbit','jumlah_penerbit_funded','jumlah_investor','jumlah_investor_deviden','jumlah_deviden_penerima','jumlah_dana','jumlah_deviden','jenis','lokasi_pemodal','lokasi_penerbit','jen','lok','lokkota'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StorepenyelenggaraRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // $index = 0;
        // $indexk = 0;
        // $d = array();
        // $r = array();
        // foreach($request->jenis as $y){
        //     $d[] = $y;
        // }
        // foreach($request->jum as $f){
        //     $r[] = $f;
        // }

        // // $jenis = array(
        // //     $d=>$r
        // // );

        
        // foreach($request->jenis as $y){
        //         $data['jenis'][][] = $y;
        // }
        //     foreach($request->jum as $f){
        //         $data['jenis'][][] =   
        //         $f;
        // }

        // $jenis = [
        //     "Property" => 1,
        //     "Food And Beverage" => 45,
        //     "Peternakan" => 16,
        //     "Perkebunan/Argo" => 2,
        //     "Service/Layanan" => 7,
        //     "Manufaktur/Produksi" => 2,
        //     "Retail/Distribusi/Logistik" => 18,
        // ];

        // $jen = [
        //     '0' => 0,
        //     '0' => 12
        // ];

        // foreach($request->kode as $y){
        //     foreach($request->jum as $f){
        //         $tes[] = [$y,$f];
        //     }
        // }

        // $data1 = array(
        //     array('qw','12')
        // );
        
        // return response()->json($tes);
        // return response()->json($r);

        // for($i=0; $i<count($request->kode); $i++){
        //     echo $request->kode;
        // }

        // return response()->json($data);
        // $data1 = array(
        //     'jenis' => $request->jenis,
        //     'jum' => $request->jum
        //     );

        // // for ($row = 0; $row < 5; $row++) {
        // //     $d[] =$data[$row];
        // //     // for ($col = 0; $col < 4; $col++) {
        // //     //     $d[][] = $data[$col];
        // //     // }
        // // }

        // $i=0;
        // foreach ($request->jenis as $key => $row) { 
        //     $jenis[] = $row;
        //     $jenis[][] = $row;
        //     $i++;
        //     }

        // $data = array(
        //     'nama perusahaan'=>$request->nama_pt,
        //     'jenis' => urlencode(base64_encode($request->jenis)),
        // );

        // // 'jenis[]id'

        // // return response()->json($request->jenis);
        // print_r($data);

        

        for($i = 0; $i < count($request->jenis);$i++){
            // $d[] = $request->jenis[$i];
            // $d[][] = $request->jum[$i];
            $jenis[] = array(
                "jenis" => $request->jenis[$i],
                "jumlah" => $request->jum[$i],
            );
        }
        for($i = 0; $i < count($request->neg);$i++){
            // $d[] = $request->jenis[$i];
            // $d[][] = $request->jum[$i];
            $lpmd[] = array(
                "id_negara" => $request->neg[$i],
                "id_kota" => 3471,
                "id_provinsi" => $request->prov[$i],
                "jumlah" => $request->j[$i],
            );
        }
        for($i = 0; $i < count($request->negp);$i++){
            // $d[] = $request->jenis[$i];
            // $d[][] = $request->jum[$i];
            $lpnb[] = array(
                "id_negara" => $request->negp[$i],
                "id_kota" => $request->kotp[$i],
                "id_provinsi" => $request->provp[$i],
                "jumlah" => $request->jp[$i],
            );
        }

        // function convert_multi_array($array) {
        //     $out = implode("&",array_map(function($a) {return implode("~",$a);},$array));
        //     print_r($out);
        //   }
        

        $data = array(
            'nama_pt'=>$request->nama_pt,
            'nama_brand'=>$request->nama_brand,
            'jumlah_penerbit'=>$request->jpnb,
            'jumlah_penerbit_funded'=>$request->jpnbf,
            'jumlah_investor'=>$request->jinv,
            'jumlah_investor_dividen'=> $request->jinvd,
            'jumlah_dana'=>$request->jdana,
            'jumlah_dividen'=>$request->jdev,
            'jumlah_dividen_penerima'=>$request->jdevp,
            'jenis' => $jenis,
            'lokasi_pemodal' => $lpmd,
            'lokasi_penerbit' => $lpnb,
        );


            $tkn = akun::where('user_id',Auth::user()->id)->first();

            $body = json_encode($data, JSON_UNESCAPED_SLASHES);
            $headers = array(
                'Token:'.$tkn->token
            );
            $curl = curl_init();
            
            curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://aludi.id/index.php/api/v1/penyelenggara/pulling',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => http_build_query($data),
            CURLOPT_HTTPHEADER => $headers,
            ));
            
            $response = curl_exec($curl);
            
            curl_close($curl);
            $result = json_decode($response, TRUE);


        $p = new penyelenggara();
        $p->nama_pt = $request->nama_pt;
        $p->nama_brand = $request->nama_brand;
        $p->jumlah_penerbit = $request->jpnb;
        $p->jumlah_penerbit_funded = $request->jpnbf;
        $p->jumlah_investor = $request->jinv;
        $p->jumlah_investor_dividen = $request->jinvd;
        $p->jumlah_dana = $request->jdana;
        $p->jumlah_dividen = $request->jdev;
        $p->jumlah_dividen_penerima = $request->jdevp;
        $p->save();

        for($i = 0; $i < count($request->jenis);$i++){
            $row                        = NEW jenis();
            $row->jid                   = $p->id;
            $row->jenis                 = $request->jenis[$i];
            $row->jumlah                = $request->jum[$i];
            $row->save();
        };

        for($i = 0; $i < count($request->neg);$i++){
            $rowp = NEW prov_sbt();
            $rowp->pid = $p->id; 
            $rowp->negid = $request->neg[$i]; 
            $rowp->provid = $request->prov[$i]; 
            $rowp->kotid = 3471; 
            $rowp->jumlah = $request->j[$i]; 
            $rowp->save();
        }

        for($i = 0; $i < count($request->negp);$i++){
            $rowpn = NEW provp_sbt();
            $rowpn->pid = $p->id ; 
            $rowpn->negid = $request->negp[$i]; 
            $rowpn->provid = $request->provp[$i]; 
            $rowpn->kotid = $request->kotp[$i]; 
            $rowpn->jumlah = $request->jp[$i]; 
            $rowpn->save();
        }

        // $e = serialize($da);
        // $r = unserialize($e);
        // echo $r;
        // return response()->json($data);
        $notif = array(
            'message' => 'Data success submited',
            'alert-type' => 'success'
        );
        // // DB::rollBack();
        // // print_r($result);
        return redirect('/penyelenggara')->with($notif);
        //     // dd($headers);

        // echo json_encode($data);
        // print_r($request->jinvd);
        // return response()->json($data);
        // echo http_build_query($data);
        // return response()->json($data);
    }

    public function cron_penyelenggara(){


        $jenis = db::table('santarax_db.emitens as em')
        ->select('c.category as cat',db::raw('count(em.id) as jcat'),'c.id as cid')
        ->join('santarax_db.emiten_journeys as ej','ej.emiten_id','=','em.id')
        ->join('santarax_db.categories as c','em.category_id','=','c.id')
        // ->join('project_santara.jenis as jen','c.id','=','jen.kode')
        ->whereRaw("em.is_deleted = 0 and em.is_active=1 and ej.title = 'Pendanaan Terpenuhi'")
        ->groupBy('em.category_id')->get();

        // select tra.province, count(tra.province) from traders tra
        // join transactions ons on ons.trader_id = ons.id
        // where tra.is_verified = 1 and tra.is_deleted = 0 and tra.tnc = 1 and ons.is_deleted = 0 and ons.is_verified = 1
        // group by province;

        // $jen = db::table('project_santara.jenis as je')
        // ->select('je.kode','je,kode_ald')
        // ->where('je.kode',12)->get();

        $jen = db::connection('mysql')->table('jenis')
                                    ->select('*')
                                    ->get();
        foreach($jenis as $i){
            foreach($jen as $a){
                if ($i->cid == $a->kode) {
                    # code...
                    $jj[] = array(
                        "jenis" => $a->kode,
                        "jumlah" => $i->jcat,
                    );
                }
            };
        };

        $lok = db::connection('mysql')->table('prov')->select('*')->get();
        $lokkota = db::connection('mysql')->table('kotas')->select('*')->get();

        foreach($lok as $i){
            $d[] = $i->prov;
            };
        $lokasi_pemodal = db::connection('mysql2')->table('traders as tra')
        ->select('tra.province as prov', db::raw('count(tra.province) as jum'))
        ->join('transactions as ons','ons.trader_id' ,'=', 'ons.id')
        ->whereRaw("tra.is_verified = 1 and tra.is_deleted = 0 and tra.tnc = 1 and ons.is_deleted = 0 and ons.is_verified = 1")
        ->groupBy("province")
        ->whereIn('tra.province',$d)
        ->get();

        foreach($lokasi_pemodal as $i){
            foreach($lok as $a){
                // $d[] = $request->jenis[$i];
                // $d[][] = $request->jum[$i];
                    if(strtolower($i->prov) == strtolower($a->prov)){
                    $lpmd[] = array(
                        "id_negara" => 'ID',
                        "id_kota" => 3471,
                        "id_provinsi" => $a->kode_ald,
                        "jumlah" => $i->jum,
                    );
                }
            }
        }


        $lokasi_penerbit = db::connection('mysql2')->table('emitens as e')
        ->select('p.name as prov','r.name as reg','p.id as pid','r.id as rid',db::raw('count(e.id) as jum'))
        ->join('regencies as r','e.regency_id','=','r.id')
        ->join('provinces as p','p.id','=','r.province_id')
        ->whereRaw('e.is_deleted = 0 and e.is_active=1')
        ->groupBy('r.id')
        ->get();

        foreach($lokasi_penerbit as $i){
            foreach($lok as $a){
                foreach($lokkota as $k){
                    if ($i->rid == $k->id){
                       if ($i->pid == $a->kode){
                        $lpnb[] = array(
                            "id_negara" => 'ID',
                            "id_kota" => $k->kode,
                            "id_provinsi" => $a->kode_ald,
                            "jumlah" => $i->jum,
                        );
                        }
                    }
                }
            }
        }
        // for($i = 0; $i < count($request->negp);$i++){
        //     // $d[] = $request->jenis[$i];
        //     // $d[][] = $request->jum[$i];
        //     $lpnb[] = array(
        //         "id_negara" => $request->negp[$i],
        //         "id_kota" => $request->kotp[$i],
        //         "id_provinsi" => $request->provp[$i],
        //         "jumlah" => $request->jp[$i],
        //     );
        // }


        // for($i = 0; $i < count($cid);$i++){
        //     // for($i = 0; $i < count($jenis->cat);$i++){
        //     // $d[] = $request->jenis[$i];
        //     // $d[][] = $request->jum[$i];
        //     // if($jenis->cid == $jen->kode){
        //     for($a = 0; $a < count($kd);$a++){
        //         if ($cid == $kd) {
        //             # code...
        //             $jcron[] = array(
        //                 "[".$i."][jenis]" => $kd[$a][$a][$a],
        //                 "[".$i."][jumlah]" => $jcat[$i],
        //             );
        //         }
        //     }
        //     // }
        //     // }
        // }
        // for($i = 0; $i < count($request->neg);$i++){
        //     // $d[] = $request->jenis[$i];
        //     // $d[][] = $request->jum[$i];
        //     $lpmd[] = array(
        //         "[".$i."][id_negara]" => $request->neg[$i],
        //         "[".$i."][id_provinsi]" => $request->prov[$i],
        //         "[".$i."][jumlah]" => $request->j[$i],
        //     );
        // }
        // for($i = 0; $i < count($request->negp);$i++){
        //     // $d[] = $request->jenis[$i];
        //     // $d[][] = $request->jum[$i];
        //     $lpnb[] = array(
        //         "[".$i."][id_negara]" => $request->negp[$i],
        //         "[".$i."][id_provinsi]" => $request->provp[$i],
        //         "[".$i."][jumlah]" => $request->jp[$i],
        //     );
        // }
        // $d[] = array($jenis->cat);

        $jumlah_penerbit = db::connection('mysql2')->table('emitens as e')->select("e.id")
        ->join('emiten_journeys as ej','ej.emiten_id','=','e.id')
        ->whereraw('e.is_deleted = 0 and e.is_active=1')
        ->groupBy('e.id')
        ->get();
        $jumlah_penerbit_funded = db::connection('mysql2')->table('emitens as e')->select("e.id")
        ->join('emiten_journeys as ej','ej.emiten_id','=','e.id')
        ->whereraw("e.is_deleted = 0 and e.is_active=1 and ej.title = 'Pendanaan Terpenuhi'")
        ->groupBy('e.id')
        ->get();

        // SELECT COUNT(ers.email) total_pemodal_total 
        // FROM users ers
        // JOIN traders tra on tra.user_id = ers.id
        // WHERE EXISTS (SELECT * FROM transactions ons 
        // WHERE ons.trader_id = tra.id AND ons.is_verified = 1 AND ons.is_deleted = 0 )

        $jumlah_investor = db::connection('mysql2')->table('users as ers')->select(db::raw("COUNT(ers.email) as total_pemodal_total"))
        ->join('traders as tra', 'tra.user_id','=','ers.id')
        ->whereRaw("EXISTS (SELECT * FROM transactions ons 
        WHERE ons.trader_id = tra.id AND ons.is_verified = 1 AND ons.is_deleted = 0 )")->first();

        // SELECT sum(b.devidend) from bagihasils as b
        // INNER JOIN traders as t on t.id = b.trader_id
        // WHERE EXISTS (SELECT * FROM transactions ons 
        // WHERE ons.trader_id = t.id AND ons.is_verified = 1 AND ons.is_deleted = 0 )

        $jumlah_investor_deviden = db::connection('mysql2')->table('bagihasils as b')
        ->select(db::raw('sum(b.devidend) as jid'))
        ->join('traders as t','t.id' ,'=','b.trader_id')
        ->whereraw('EXISTS (SELECT * FROM transactions ons WHERE ons.trader_id = t.id AND ons.is_verified = 1 AND ons.is_deleted = 0 )')
        ->first();

        $jumlah_deviden_penerima = db::connection('mysql2')->table('bagihasils as b')
        ->select(db::raw('sum(b.devidend) as jid'))
        ->join('traders as t','t.id' ,'=','b.trader_id')
        ->whereraw('EXISTS (SELECT * FROM transactions ons WHERE ons.trader_id = t.id AND ons.is_verified = 1 AND ons.is_deleted = 0 )')
        ->first();

//         SELECT SUM(amount) total_pendanaan
// FROM transactions 
// WHERE is_verified = 1 AND is_deleted = 0

        $jumlah_dana = db::connection('mysql2')->table('transactions as t')->select(db::raw('SUM(amount) as total_pendanaan'))->whereRaw('is_verified = 1 AND is_deleted = 0')->first();

//         SELECT sum(d.devidend) from devidend as d
// INNER JOIN emitens as e on e.id= d.emiten_id
        $jumlah_deviden = db::connection('mysql2')->table('devidend as d')->select(db::raw('sum(d.devidend) as d'))
        ->join('emitens as e','e.id','=','d.emiten_id')->first();

        $data = array(
            'nama_pt'=>'Santara Daya Inspiratama',
            'nama_brand'=>'Santara',
            'jumlah_penerbit'=>count($jumlah_penerbit),
            'jumlah_penerbit_funded'=>count($jumlah_penerbit_funded),
            'jumlah_investor'=> $jumlah_investor->total_pemodal_total,
            'jumlah_investor_dividen'=>  round($jumlah_investor_deviden->jid,0),
            'jumlah_dana'=>round($jumlah_dana->total_pendanaan,0),
            'jumlah_dividen'=>round($jumlah_deviden->d,0),
            'jumlah_dividen_penerima'=>round($jumlah_investor_deviden->jid,0),
            'jenis' => $jj,
            'lokasi_pemodal' => $lpmd,
            'lokasi_penerbit' => $lpnb,
        );

        $tkn = akun::where('user_id',1)->first();

            $body = json_encode($data, JSON_UNESCAPED_SLASHES);
            $headers = array(
                'Token:'.$tkn->token
            );
            $curl = curl_init();
            
            curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://aludi.id/index.php/api/v1/penyelenggara/pulling',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => http_build_query($data),
            CURLOPT_HTTPHEADER => $headers,
            ));
            
            $response = curl_exec($curl);
            
            curl_close($curl);
            $result = json_decode($response, TRUE);


            $p = new penyelenggara();
            $p->nama_pt = 'Santara Daya Inspiratama';
            $p->nama_brand = 'Santara';
            $p->jumlah_penerbit = count($jumlah_penerbit);
            $p->jumlah_penerbit_funded = count($jumlah_penerbit_funded);
            $p->jumlah_investor = $jumlah_investor->total_pemodal_total;
            $p->jumlah_investor_dividen = round($jumlah_investor_deviden->jid,0);
            $p->jumlah_dana = round($jumlah_dana->total_pendanaan,0);
            $p->jumlah_dividen = round($jumlah_deviden->d,0);
            $p->jumlah_dividen_penerima = round($jumlah_investor_deviden->jid,0);
            $p->save();

            foreach($jenis as $i){
                foreach($jen as $a){
                    if ($i->cid == $a->kode) {
                    $row                        = NEW jenis();
                    $row->jid                   = $p->id;
                    $row->jenis                 = $a->jenis_ald;
                    $row->jumlah                = $i->jcat;
                    $row->save();
                    }
                }
            };

            foreach($lokasi_pemodal as $i){
                foreach($lok as $a){
                    // $d[] = $request->jenis[$i];
                    // $d[][] = $request->jum[$i];
                        if(strtolower($i->prov) == strtolower($a->prov)){
                            $rowp = NEW prov_sbt();
                            $rowp->pid = $p->id; 
                            $rowp->negid = 'ID'; 
                            $rowp->provid = $a->kode_ald; 
                            $rowp->kotid = 3471; 
                            $rowp->jumlah = $i->jum; 
                            $rowp->save();
                    }
                }
            }

            foreach($lokasi_penerbit as $i){
                foreach($lok as $a){
                    foreach($lokkota as $k){
                        if ($i->rid == $k->id){
                           if ($i->pid == $a->kode){
                                $rowpn = NEW provp_sbt();
                                $rowpn->pid = $p->id ; 
                                $rowpn->negid = 'ID'; 
                                $rowpn->provid = $a->kode_ald; 
                                $rowpn->kotid = $k->kode; 
                                $rowpn->jumlah = $i->jum; 
                                $rowpn->save();
                        }
                    }
                }
            }
        }

        // echo $response;
        return response()->json(['status'=>'scheduler penyelenggara berhasil!!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\penyelenggara  $penyelenggara
     * @return \Illuminate\Http\Response
     */
    public function show(penyelenggara $penyelenggara)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\penyelenggara  $penyelenggara
     * @return \Illuminate\Http\Response
     */
    public function edit(penyelenggara $penyelenggara)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdatepenyelenggaraRequest  $request
     * @param  \App\Models\penyelenggara  $penyelenggara
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatepenyelenggaraRequest $request, penyelenggara $penyelenggara)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\penyelenggara  $penyelenggara
     * @return \Illuminate\Http\Response
     */
    public function destroy(penyelenggara $penyelenggara)
    {
        //
    }
}
