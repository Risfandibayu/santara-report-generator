<?php

namespace App\Http\Controllers;

use App\Models\penerbit;
use App\Models\penerbit_kode;
use App\Http\Requests\StorepenerbitRequest;
use App\Http\Requests\UpdatepenerbitRequest;
use App\Models\akun;
use App\Models\dtlog;
use App\Models\status;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;

use function PHPUnit\Framework\isEmpty;

class PenerbitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        
        $penerbit_submited = db::connection('mysql')->table('penerbits')->select('penerbits.*','statuses.status as sta')
        ->join('statuses','statuses.kode','=','penerbits.status')
        ->get();

        $id = db::connection('mysql')->table('penerbits')->select('eid')->get();

        $d = array();
        foreach($id as $i){
        $d[] = $i->eid;
        };
        

        // created_at > 
        // (select date from emiten_journeys WHERE emiten_journeys.emiten_id=eid and title = 'Pendanaan Terpenuhi')
        // and
        $penerbit = db::connection('mysql2')
        ->table('emitens')
        ->select('emitens.id as eid','t.id as tid','u.id as uid','c.id as cid','t.name as name','t.phone as phone','u.email as email','emitens.company_name as com','emitens.trademark as tm','c.category as cat',
        db::raw("(
        select sum(transactions.amount) 
        from transactions 
        where 
        transactions.emiten_id = eid) as dan")
        ,'emitens.business_description as des', db::raw("(emitens.price * emitens.supply) as dana"))
        ->join('traders as t', 'emitens.trader_id','=' ,'t.id')
        ->join('users as u', 't.user_id','=','u.id')
        ->join('categories as c','emitens.category_id' ,'=' ,'c.id')
        // ->LEFTjoin('transactions','transactions.emiten_id','=','emitens.id')
        // ->LEFTjoin('emiten_journeys as ej','ej.emiten_id','=','emitens.id' )
        // ->take(10)
        // ->where(db::raw("(select sum(tr.amount) from 
        // (select * from transactions where created_at > '2021-06-01 00:00:00') as tr 
        // inner join 
        // (select * from emitens where is_active=1) as em 
        // on tr.emiten_id=em.id 
        // where em.id=emitens.id)"), '!=',NULL)
        ->whereNotIn('emitens.id',$d)
        // ->where('transactions.created_at','>','ej.date')
        // ->where('ej.title','Pendanaan Terpenuhi')
        ->Where( 'emitens.is_active','=', '1')
        // ->whereRaw("transactions.created_at > (select ej.date WHERE ej.title = 'Pendanaan Terpenuhi')")
        ->where( 'emitens.is_deleted','=' ,'0')
        // ->whereRaw("transactions.created_at > (select ej.date WHERE ej.title = 'Pendanaan Terpenuhi')")
        ->groupBy('emitens.id')

        ->get();
        
        $c = db::connection('mysql2')->table('categories')->select('*')->where('is_deleted',0)->get();
        $s = status::select('*')->get();
        return view('penerbit.index',compact('penerbit','penerbit_submited','c','s'));
    }

    public function indexp()
    {
        //
        
        $penerbit_submited = db::connection('mysql')->table('penerbits')->select('penerbits.*','statuses.status as sta')
        ->join('statuses','statuses.kode','=','penerbits.status')
        ->get();

        $id = db::connection('mysql')->table('penerbits')->select('eid')->get();

        $d = array();
        foreach($id as $i){
        $d[] = $i->eid;
        };
        

        // created_at > 
        // (select date from emiten_journeys WHERE emiten_journeys.emiten_id=eid and title = 'Pendanaan Terpenuhi')
        // and
        $penerbit = db::connection('mysql2')
        ->table('emitens')
        ->select('emitens.id as eid','t.id as tid','u.id as uid','c.id as cid','t.name as name','t.phone as phone','u.email as email','emitens.company_name as com','emitens.trademark as tm','c.category as cat',
        db::raw("(
        select sum(transactions.amount) 
        from transactions 
        where 
        transactions.emiten_id = eid) as dan")
        ,'emitens.business_description as des', db::raw("(emitens.price * emitens.supply) as dana"))
        ->join('traders as t', 'emitens.trader_id','=' ,'t.id')
        ->join('users as u', 't.user_id','=','u.id')
        ->join('categories as c','emitens.category_id' ,'=' ,'c.id')
        // ->LEFTjoin('transactions','transactions.emiten_id','=','emitens.id')
        // ->LEFTjoin('emiten_journeys as ej','ej.emiten_id','=','emitens.id' )
        // ->take(10)
        // ->where(db::raw("(select sum(tr.amount) from 
        // (select * from transactions where created_at > '2021-06-01 00:00:00') as tr 
        // inner join 
        // (select * from emitens where is_active=1) as em 
        // on tr.emiten_id=em.id 
        // where em.id=emitens.id)"), '!=',NULL)
        // ->whereNotIn('emitens.id',$d)
        // ->where('transactions.created_at','>','ej.date')
        // ->where('ej.title','Pendanaan Terpenuhi')
        ->Where( 'emitens.is_active','=', '1')
        // ->whereRaw("transactions.created_at > (select ej.date WHERE ej.title = 'Pendanaan Terpenuhi')")
        ->where( 'emitens.is_deleted','=' ,'0')
        // ->whereRaw("transactions.created_at > (select ej.date WHERE ej.title = 'Pendanaan Terpenuhi')")
        ->groupBy('emitens.id')

        ->get();
        
        $c = db::connection('mysql2')->table('categories')->select('*')->where('is_deleted',0)->get();
        $s = status::select('*')->get();
        return view('penerbit.indexp',compact('penerbit','penerbit_submited','c','s'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StorepenerbitRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorepenerbitRequest $request)
    {   
        DB::beginTransaction();
            try {
            
                if($request->input('deskripsi') == " "){
                    $d = $request->input('deskripsi');
                }else{
                    $d = "-";
                }

            $data = array(
                'owner'=>$request->input('owner'),
                'nomor_telepon'=>$request->input('no_telp'),
                'email'=>$request->input('email'),
                'nama_perusahaan'=>$request->input('nama_pt'),
                'nama_brand'=>$request->input('nama_brand'),
                'bidang_usaha'=>$request->input('bidang_usaha'),
                'total_pendanaan'=>$request->input('total_pendanaan'),
                'deskripsi'=>$d,
                'status'=>39
            );

            $tkn = akun::where('user_id',Auth::user()->id)->first();

            $body = json_encode($data, JSON_UNESCAPED_SLASHES);
            $headers = array(
                'Content-Type: multipart/form-data',
                'Token:'.$tkn->token
            );
            $curl = curl_init();
            
            curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://aludi.id/index.php/api/v1/penerbit/created',
            // CURLOPT_URL => 'https://my.ipaymu.com/api/v2/register',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTPHEADER => $headers,
            ));
            
            $response = curl_exec($curl);
            
            curl_close($curl);
            $result = json_decode($response, TRUE);
            

            if($result['respon']['kode'] == '201'){
            $row                        = NEW penerbit();
            $row->eid                  = $request->input('eid');
            $row->tid                  = $request->input('tid');
            $row->uid                  = $request->input('uid');
            $row->cid                  = $request->input('cid');
            $row->owner                = $request->input('owner');
            $row->no_telp              = $request->input('no_telp');
            $row->email              = $request->input('email');
            $row->nama_pt              = $request->input('nama_pt');
            $row->nama_brand              = $request->input('nama_brand');
            $row->bidang_usaha              = $request->input('bidang_usaha');
            $row->total_pendanaan              = $request->input('total_pendanaan');
            $row->deskripsi              = $request->input('deskripsi');
            $row->status              = $request->input('status');
            $row->save();

            $dt                        = NEW dtlog();
            $dt->eid                  = $request->input('eid');
            $dt->tid                  = $request->input('tid');
            $dt->uid                  = $request->input('uid');
            $dt->cid                  = $request->input('cid');
            $dt->owner                = $request->input('owner');
            $dt->no_telp              = $request->input('no_telp');
            $dt->email              = $request->input('email');
            $dt->nama_pt              = $request->input('nama_pt');
            $dt->nama_brand              = $request->input('nama_brand');
            $dt->bidang_usaha              = $request->input('bidang_usaha');
            $dt->total_pendanaan              = $request->input('total_pendanaan');
            $dt->deskripsi              = $request->input('deskripsi');
            $dt->status              = $request->input('status');
            $dt->ket              = "PUSH DATA PENERBIT";
            $dt->timelog              = date('Y-m-d H:i:s');
            $dt->save();
            
           
            // echo $response;
            $pkode = new penerbit_kode();
            $pkode->eid = $request->input('eid');
            $pkode->kode = $result['data']['kode'];
            $pkode->save();

            $notif = array(
                'message' => 'Data Success Submited!',
                'alert-type' => 'success'
            );
            DB::commit();
            // print_r($result['data']['kode']);
            return redirect('/penerbit')->with($notif);
            }else{
            $notif = array(
                'message' => 'Data failed Submited!',
                'alert-type' => 'error'
            );
            DB::rollBack();
            print_r($result);
            return redirect('/penerbit')->with($notif);
            }
        } catch (ClientException $e) {
            DB::rollback();
            Log::info($e->getMessage());
            $notif = array(
                'message' => 'Error!',
                'alert-type' => 'error'
            );
            return redirect('/penerbit')->with($notif);
        }
    }
    
    public function cron_penerbit(){
        
        $id = db::connection('mysql')->table('penerbits')->select('eid')->get();
        $psbt = db::connection('mysql')->table('penerbits')->get();
    
        $d = array();
        foreach($id as $i){
        $d[] = $i->eid;
        };

        $pen = db::connection('mysql2')
        ->table('emitens')
        ->select('emitens.id as eid','t.id as tid','u.id as uid','c.id as cid','t.name as name','t.phone as phone','u.email as email','emitens.company_name as com','emitens.trademark as tm','c.category as cat',
        db::raw("(
        select sum(transactions.amount) 
        from transactions 
        where 
        transactions.emiten_id = eid) as dana")
        ,'emitens.business_description as des')
        ->join('traders as t', 'emitens.trader_id','=' ,'t.id')
        ->join('users as u', 't.user_id','=','u.id')
        ->join('categories as c','emitens.category_id' ,'=' ,'c.id')
        // ->LEFTjoin('transactions','transactions.emiten_id','=','emitens.id')
        // ->LEFTjoin('emiten_journeys as ej','ej.emiten_id','=','emitens.id' )
        // ->take(10)
        // ->where(db::raw("(select sum(tr.amount) from 
        // (select * from transactions where created_at > '2021-06-01 00:00:00') as tr 
        // inner join 
        // (select * from emitens where is_active=1) as em 
        // on tr.emiten_id=em.id 
        // where em.id=emitens.id)"), '!=',NULL)
        // ->whereNotIn('emitens.id',$d)
        // ->where('transactions.created_at','>','ej.date')
        // ->where('ej.title','Pendanaan Terpenuhi')
        ->Where( 'emitens.is_active','=', '1')
        // ->whereRaw("transactions.created_at > (select ej.date WHERE ej.title = 'Pendanaan Terpenuhi')")
        ->where( 'emitens.is_deleted','=' ,'0')
        // ->whereRaw("transactions.created_at > (select ej.date WHERE ej.title = 'Pendanaan Terpenuhi')")
        ->groupBy('emitens.id')
        ->get();

        DB::beginTransaction();
        try {
        $r = array();
        // foreach($pen as $p => $key){
        //     if(in_array($key->eid,$d)){
        //         $r[] = 1;
        //     }else{
        //         $r[] = 2;
        //     }
        // }
        foreach($pen as $p => $key){
                if(in_array($key->eid,$d)){
        //             // $ddd[] = 2;

        //             // $p = penerbit::where('id',$id)->first();
                    $pc = penerbit_kode::where('eid',(string) $key->eid)->first();
                    
                    $data = array(
                        'kode'=>$pc->kode,
                        'owner'=>$key->name,
                        'nomor_telepon'=>$key->phone,
                        'email'=>$key->email,
                        'nama_perusahaan'=>$key->com,
                        'nama_brand'=>$key->tm,
                        'bidang_usaha'=>$key->cat,
                        'total_pendanaan'=>$key->dana,
                        'deskripsi'=>$key->des,
                        'status'=>39
                    );
                    
                    $tkn = akun::where('user_id',1)->first();
                    $body = json_encode($data, JSON_UNESCAPED_SLASHES);
                    $headers = array(
                            'Content-Type: multipart/form-data',
                            'Token:'.$tkn->token
                        );
                    $curl = curl_init();
                    
                    curl_setopt_array($curl, array(
                    CURLOPT_URL => 'https://aludi.id/index.php/api/v1/penerbit/updated',
                    // CURLOPT_URL => 'https://my.ipaymu.com/api/v2/register',
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS => $data,
                    CURLOPT_HTTPHEADER => $headers,
                    ));
                    
                    $response = curl_exec($curl);
                    
                    curl_close($curl);
                    $result = json_decode($response, TRUE);
                    $r[] = $response;
                    if($result['respon']['kode'] == '201'){
                    $row                       = penerbit::where('eid',(string) $key->eid)->first();
                    $row->eid                  = $key->eid;
                    $row->tid                  = $key->tid;
                    $row->uid                  = $key->uid;
                    $row->cid                  = $key->cid;
                    $row->owner                = $key->name;
                    $row->no_telp              = $key->phone;
                    $row->email                = $key->email;
                    $row->nama_pt              = $key->com;
                    $row->nama_brand           = $key->tm;
                    $row->bidang_usaha         = $key->cat;
                    $row->total_pendanaan      = $key->dana;
                    $row->deskripsi            = $key->des;
                    $row->status               = 39;
                    $row->save();
 
                    $dt                        = NEW dtlog();
                    $dt->eid                  = $key->eid;
                    $dt->tid                  = $key->tid;
                    $dt->uid                  = $key->uid;
                    $dt->cid                  = $key->cid;
                    $dt->owner                = $row->owner;
                    $dt->no_telp              = $row->no_telp;
                    $dt->email                = $row->email;
                    $dt->nama_pt              = $row->nama_pt;
                    $dt->nama_brand           = $row->nama_brand;
                    $dt->bidang_usaha         = $row->bidang_usaha;
                    $dt->total_pendanaan      = $row->total_pendanaan;
                    $dt->deskripsi            = $row->deskripsi;
                    $dt->status               = $row->status ;
                    $dt->ket                  = 'SCHEDULER UPDATE DATA PENERBIT';
                    $dt->timelog              = date('Y-m-d H:i:s');
                    $dt->save();

                    DB::commit();
                    }
                }else{
                    if($key->des == " "){
                        $d = $key->des;
                    }else{
                        $d = "-";
                    }
    
                    $data = array(
                    'owner'=>$key->name,
                    'nomor_telepon'=>(int) $key->phone,
                    'email'=>$key->email,
                    'nama_perusahaan'=>$key->com,
                    'nama_brand'=>$key->tm,
                    'bidang_usaha'=>$key->cat,
                    'total_pendanaan'=>(int) $key->dana,
                    'deskripsi'=>$d,
                    'status'=>39
                    );
        
                    $tkn = akun::where('user_id',Auth::user()->id)->first();
        
                    $body = json_encode($data, JSON_UNESCAPED_SLASHES);
                    $headers = array(
                        'Content-Type: multipart/form-data',
                        'Token:'.$tkn->token
                    );
                    $curl = curl_init();
                    
                    curl_setopt_array($curl, array(
                    CURLOPT_URL => 'https://aludi.id/index.php/api/v1/penerbit/created',
                    // CURLOPT_URL => 'https://my.ipaymu.com/api/v2/register',
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS => $data,
                    CURLOPT_HTTPHEADER => $headers,
                    ));
                    
                    $response = curl_exec($curl);
                    
                    curl_close($curl);
                    $result = json_decode($response, TRUE);

                    $r[] = $response;
                    if($result['respon']['kode'] == '201'){
                        $row                       = new penerbit();
                        $row->eid                  = $key->eid;
                        $row->tid                  = $key->tid;
                        $row->uid                  = $key->uid;
                        $row->cid                  = $key->cid;
                        $row->owner                = $key->name;
                        $row->no_telp              = $key->phone;
                        $row->email                = $key->email;
                        $row->nama_pt              = $key->com;
                        $row->nama_brand           = $key->tm;
                        $row->bidang_usaha         = $key->cat;
                        $row->total_pendanaan      = $key->dana;
                        $row->deskripsi            = $key->des;
                        $row->status               = 39;
                        $row->save();
                    // $ddd[] = 1;

                        $pkode = new penerbit_kode();
                        $pkode->eid = $row->eid;
                        $pkode->kode = $result['data']['kode'];
                        $pkode->save();

                    
                        $dt                        = NEW dtlog();
                        $dt->eid                  = $key->eid;
                        $dt->tid                  = $key->tid;
                        $dt->uid                  = $key->uid;
                        $dt->cid                  = $key->cid;
                        $dt->owner                = $row->owner;
                        $dt->no_telp              = $row->no_telp;
                        $dt->email                = $row->email;
                        $dt->nama_pt              = $row->nama_pt;
                        $dt->nama_brand           = $row->nama_brand;
                        $dt->bidang_usaha         = $row->bidang_usaha;
                        $dt->total_pendanaan      = $row->total_pendanaan;
                        $dt->deskripsi            = $row->deskripsi;
                        $dt->status               = $row->status ;
                        $dt->ket                  = 'SCHEDULER PUSH DATA PENERBIT';
                        $dt->timelog              = date('Y-m-d H:i:s');
                        $dt->save();
                        DB::commit();
                    
                    // echo $response;

                    }
    
                }
            }

            return response()->json(['status'=>'scheduler penerbit berhasil!!']);
        
        // return response()->json($r);
        
        } catch (ClientException $e) {
            DB::rollback();
            Log::info($e->getMessage());
        }
            // return response()->json($ddd);

    }
    


    

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\penerbit  $penerbit
     * @return \Illuminate\Http\Response
     */
    public function show(penerbit $penerbit)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\penerbit  $penerbit
     * @return \Illuminate\Http\Response
     */
    public function edit(penerbit $penerbit)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdatepenerbitRequest  $request
     * @param  \App\Models\penerbit  $penerbit
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatepenerbitRequest $request, penerbit $penerbit,$id)
    {
        //
        $p = penerbit::where('id',$id)->first();
        $pc = penerbit_kode::where('eid',$p->eid)->first();
        

        

        $data = array(
            'kode'=>$pc->kode,
            'owner'=>$request->input('owner'),
            'nomor_telepon'=>$request->input('no_telp'),
            'email'=>$request->input('email'),
            'nama_perusahaan'=>$request->input('nama_pt'),
            'nama_brand'=>$request->input('nama_brand'),
            'bidang_usaha'=>$request->input('bidang_usaha'),
            'total_pendanaan'=>$request->input('total_pendanaan'),
            'deskripsi'=>$request->input('deskripsi'),
            'status'=>39
        );
        
        $tkn = akun::where('user_id',Auth::user()->id)->first();
        $body = json_encode($data, JSON_UNESCAPED_SLASHES);
        $headers = array(
                'Content-Type: multipart/form-data',
                'Token:'.$tkn->token
            );
        $curl = curl_init();
        
        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://aludi.id/index.php/api/v1/penerbit/updated',
        // CURLOPT_URL => 'https://my.ipaymu.com/api/v2/register',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => $data,
        CURLOPT_HTTPHEADER => $headers,
        ));
        
        $response = curl_exec($curl);
        
        curl_close($curl);
        $result = json_decode($response, TRUE);

        if($result['respon']['kode'] == '201'){
            $p->owner = $request->input('owner');
            $p->no_telp = $request->input('no_telp');
            $p->email = $request->input('email');
            $p->nama_pt = $request->input('nama_pt');
            $p->nama_brand = $request->input('nama_brand');
            $p->bidang_usaha = $request->input('bidang_usaha');
            $p->total_pendanaan = $request->input('total_pendanaan');
            $p->deskripsi = $request->input('deskripsi');
            $p->save();
            
            $dt                        = NEW dtlog();
            $dt->eid                  = $p->eid;
            $dt->tid                  = $p->tid;
            $dt->uid                  = $p->uid;
            $dt->cid                  = $p->cid;
            $dt->owner                = $request->input('owner');
            $dt->no_telp              = $request->input('no_telp');
            $dt->email              = $request->input('email');
            $dt->nama_pt              = $request->input('nama_pt');
            $dt->nama_brand              = $request->input('nama_brand');
            $dt->bidang_usaha              = $request->input('bidang_usaha');
            $dt->total_pendanaan              = $request->input('total_pendanaan');
            $dt->deskripsi              = $request->input('deskripsi');
            $dt->status              = 39;
            $dt->ket              = 'UPDATE DATA PENERBIT';
            $dt->timelog              = date('Y-m-d H:i:s');
            $dt->save();

        $notif = array(
            'message' => 'Data penerbit success edited!!',
            'alert-type' => 'success'
        );
        return redirect('/penerbit')->with($notif);
        }else{
        
            $notif = array(
                'message' => 'Data penerbit failed edited!!',
                'alert-type' => 'error'
            );
            return redirect('/penerbit')->with($notif);
        }   
        // dd($pc->kode);
        // print_r($result);
    }

    public function update_status(UpdatepenerbitRequest $request,$id){
        
        $p = penerbit::where('id',$id)->first();
        $pc = penerbit_kode::where('eid',$p->eid)->first();

        

        $data = array(
            'kode'=>$pc->kode,
            'status'=>$request->input('status')
        );
        $tkn = akun::where('user_id',Auth::user()->id)->first();
        $headers = array(
            'Content-Type: multipart/form-data',
            'Token:'.$tkn->token
        );
        $curl = curl_init();
        
        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://aludi.id/index.php/api/v1/penerbit/updated_status',
        // CURLOPT_URL => 'https://my.ipaymu.com/api/v2/register',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => $data,
        CURLOPT_HTTPHEADER => $headers,
        ));
        
        $response = curl_exec($curl);
        
        curl_close($curl);
        $result = json_decode($response, TRUE);
        if($result['respon']['kode'] == '201'){

        $p->status = $request->input('status');
        $p->save();

        $dt                        = NEW dtlog();
            $dt->eid                  = $p->eid;
            $dt->tid                  = $p->tid;
            $dt->uid                  = $p->uid;
            $dt->cid                  = $p->cid;
            $dt->owner                = $p->owner;
            $dt->no_telp              = $p->no_telp;
            $dt->email              = $p->email;
            $dt->nama_pt              = $p->nama_pt;
            $dt->nama_brand              = $p->nama_brand;
            $dt->bidang_usaha              = $p->bidang_usaha;
            $dt->total_pendanaan              = $p->total_pendanaan;
            $dt->deskripsi              = $p->deskripsi;
            $dt->status              = $request->input('status');;
            $dt->ket              = 'UPDATE STATUS PENERBIT';
            $dt->timelog              = date('Y-m-d H:i:s');
            $dt->save();
        $notif = array(
            'message' => 'Status penerbit success Updated!!',
            'alert-type' => 'success'
        );
        return redirect('/penerbit')->with($notif);

        }else{
            $notif = array(
                'message' => 'Status penerbit failed Updated!!',
                'alert-type' => 'error'
            );
            return redirect('/penerbit')->with($notif);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\penerbit  $penerbit
     * @return \Illuminate\Http\Response
     */
    public function destroy(penerbit $penerbit)
    {
        //
    }

    public function investor($id){
        
        // echo 'ok'.$id;
        $inv = db::connection('mysql2')->table('transactions as ons')
        ->select('tra.id','tra.name as nm','ers.email as em','tra.phone as ph',db::raw("(
            SUM(IF(ons.is_verified = 1 AND ons.is_deleted = 0 AND ons.channel IN ( 'BANKTRANSFER', 'WALLET', 'INJECTION', 'VA', 'ONEPAY' ), ons.amount, 0))
                + 
                SUM(IF(ons.is_verified = 1 AND ons.is_deleted = 0 AND ons.channel = 'MARKET' 
                AND ons.description = 'buy' , ons.amount, 0))
                - 
                SUM(IF(ons.is_verified = 1 AND ons.is_deleted = 0 AND ons.channel = 'MARKET' 
                AND ons.description = 'sell' , ons.amount, 0))
        ) as saham"),"ama.balance as saldo")
        ->join('traders as tra','tra.id','=','ons.trader_id')
        ->join('emitens as emi','emi.id','=' ,'ons.emiten_id')
        ->leftJoin('users as ers','ers.id','=', 'tra.id')
        ->join('balance_utama as ama','ama.trader_id','=','tra.id')
        ->where('ons.is_deleted',0)
        ->where('ons.is_verified',1)
        ->where('emi.id',$id)
        ->groupBy('tra.id')
        ->get();

        $pnd = db::connection('mysql2')->table('emitens')->select('*')->where('id',$id)->first();

        return view('penerbit.investor',compact('inv','pnb'));
        // return json_encode($inv);
    }

    
}
