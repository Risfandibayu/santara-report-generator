<?php

namespace App\Http\Controllers;

use App\Models\akun;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class akunController extends Controller
{
    //
    public function set_akun(Request $request){
        
        $em = akun::where('email',$request->input('email'))->first();
        if($em){
            $notif = array(
                'message' => 'Email account has registered',
                'alert-type' => 'error'
            );
            return redirect('/home')->with($notif);
        }else{
        $ak = akun::where('user_id',Auth::user()->id)->first();
        if($ak){
            $akun = $ak;
        }else{
            $akun = new akun();
        }
        $akun->user_id = Auth::user()->id;
        $akun->email = $request->input('email');
        $akun->password = $request->input('password');
        // $akun->valid_until = date('Y-m-d').' 23:59:59';
        $akun->save();

        $notif = array(
            'message' => 'Setup akun success',
            'alert-type' => 'success'
        );
        return redirect('/home')->with($notif);
        }
    }

    public function refresh_token(Request $request){

        $s = $request->input('tes');
        $ak = akun::where('user_id',Auth::user()->id)->first();

        $data = array(
            'email'=>$ak->email,
            'password'=>$ak->password
        );
        
        $headers = array(
            'Content-Type: multipart/form-data',
        );
        $curl = curl_init();
        
        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://aludi.id/index.php/api/v1/auth_api/refresh_token',
        // CURLOPT_URL => 'https://my.ipaymu.com/api/v2/register',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => $data,
        CURLOPT_HTTPHEADER => $headers,
        ));
        
        $response = curl_exec($curl);
        
        curl_close($curl);
        $result = json_decode($response, TRUE);

        if($result['respon']['kode'] == '200'){
            
        $ak->token = $result['data']['token'];
        $ak->valid_until = date('Y-m-d').' 23:59:59';
        $ak->save();
        

        $notif = array(
            'message' => 'Refresh token berhasil!! Valid sampai : '.date('Y-m-d').' 23:59:59',
            'alert-type' => 'success'
        );
        return redirect('/home')->with($notif);
        }else{

            $notif = array(
                'message' => 'Refresh token gagal!! Silahkan Setup ulang Akun Aludi',
                'alert-type' => 'error'
            );
            return redirect('/home')->with($notif);
        }
        
        
        // print_r($result['respon']['kode']);

        // return response()->json(['status'=>'mantap']);
    }
    public function cron_refresh_token(Request $request){

        // $s = $request->input('tes');
        $ak = akun::where('user_id',1)->first();

        $data = array(
            'email'=>$ak->email,
            'password'=>$ak->password
        );
        
        $headers = array(
            'Content-Type: multipart/form-data',
        );
        $curl = curl_init();
        
        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://aludi.id/index.php/api/v1/auth_api/refresh_token',
        // CURLOPT_URL => 'https://my.ipaymu.com/api/v2/register',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => $data,
        CURLOPT_HTTPHEADER => $headers,
        ));
        
        $response = curl_exec($curl);
        
        curl_close($curl);
        $result = json_decode($response, TRUE);

        if($result['respon']['kode'] == '200'){
            
        $ak->token = $result['data']['token'];
        $ak->valid_until = date('Y-m-d').' 23:59:59';
        $ak->save();
        
        return response()->json(['status'=>'scheduler refresh token berhasil!!']);
        // $notif = array(
        //     'message' => 'Refresh token berhasil!! Valid sampai :'.date('Y-m-d').' 23:59:59',
        //     'alert-type' => 'success'
        // );
        // return redirect('/home')->with($notif);
        // }else{

        //     $notif = array(
        //         'message' => 'Refresh token gagal!! silahkan setup ulang Akun Aludi',
        //         'alert-type' => 'error'
        //     );
        //     return redirect('/home')->with($notif);
        // }
        
        
        // print_r($result['respon']['kode']);
        }
    }
}