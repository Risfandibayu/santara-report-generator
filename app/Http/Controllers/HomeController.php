<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','verified']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $penerbit_submited = db::connection('mysql')->table('penerbits')->select('penerbits.*','statuses.status as sta')
        ->join('statuses','statuses.kode','=','penerbits.status')
        ->get();

        $id = db::connection('mysql')->table('penerbits')->select('eid')->get();

        $d = array();
        foreach($id as $i){
        $d[] = $i->eid;
        };
        

        // created_at > 
        // (select date from emiten_journeys WHERE emiten_journeys.emiten_id=eid and title = 'Pendanaan Terpenuhi')
        // and
        $penerbit = db::connection('mysql2')
        ->table('emitens')
        ->select('emitens.id as eid','t.id as tid','u.id as uid','c.id as cid','t.name as name','t.phone as phone','u.email as email','emitens.company_name as com','emitens.trademark as tm','c.category as cat',
        db::raw("(
        select sum(transactions.amount) 
        from transactions 
        where 
        
        transactions.emiten_id = eid) as dana")
        ,'emitens.business_description as des')
        ->join('traders as t', 'emitens.trader_id','=' ,'t.id')
        ->join('users as u', 't.user_id','=','u.id')
        ->join('categories as c','emitens.category_id' ,'=' ,'c.id')
        // ->LEFTjoin('transactions','transactions.emiten_id','=','emitens.id')
        // ->LEFTjoin('emiten_journeys as ej','ej.emiten_id','=','emitens.id' )
        // ->take(10)
        // ->where(db::raw("(select sum(tr.amount) from 
        // (select * from transactions where created_at > '2021-06-01 00:00:00') as tr 
        // inner join 
        // (select * from emitens where is_active=1) as em 
        // on tr.emiten_id=em.id 
        // where em.id=emitens.id)"), '!=',NULL)
        ->whereNotIn('emitens.id',$d)
        // ->where('transactions.created_at','>','ej.date')
        // ->where('ej.title','Pendanaan Terpenuhi')
        ->Where( 'emitens.is_active','=', '1')
        // ->whereRaw("transactions.created_at > (select ej.date WHERE ej.title = 'Pendanaan Terpenuhi')")
        ->where( 'emitens.is_deleted','=' ,'0')
        // ->whereRaw("transactions.created_at > (select ej.date WHERE ej.title = 'Pendanaan Terpenuhi')")
        ->groupBy('emitens.id')

        ->get();
        return view('home',compact('penerbit','penerbit_submited'));
    }
}
