
@extends('auth.master')

@section('content')


<body class="hold-transition login-page" style="background-color: #191919">
    <div class="login-box">
      <div class="login-logo">
        <img src="{{asset('public/asset')}}/dist/img/logo_header.png"><br>
        <a href="/login" style="color: white"><b>Santara</b> API Management</a>
      </div>
      <!-- /.login-logo -->
      <div class="card">
        <div class="card-body login-card-body">
          
    
          <!-- /.social-auth-links -->
          <div class="card-header">{{ __('Confirm Password') }}</div>

                <div class="card-body">
                    {{ __('Please confirm your password before continuing.') }}

                    <form method="POST" action="{{ route('password.confirm') }}">
                        @csrf

                        <div class="row mb-3">
                            <label for="password" class="col-md-4 col-form-label text-md-end">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-sm btn-secondary">
                                    {{ __('Confirm Password') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
          
          {{-- <p class="mb-0">
            <a href="register.html" class="text-center">Register </a>
          </p> --}}
        </div>
        <!-- /.login-card-body -->
      </div>
    </div>
    <!-- /.login-box -->
    
    <!-- jQuery -->
    <script src="{{asset('public/asset/')}}pluginsjquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="{{asset('public/asset/')}}pluginsbootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('public/asset/')}}dist/jsadminlte.min.js"></script>
    </body>
@endsection
