


@extends('auth.master')

@section('content')


<body class="hold-transition login-page" style="background-color: #191919;">
    <div class="login-box">
      <div class="login-logo">
        <img src="{{asset('public/asset')}}/dist/img/logo_header.png"><br>
        <a href="/login" style="color: white"><b>Santara</b> API Management</a>
      </div>
      <!-- /.login-logo -->
      <div class="card">
        <div class="card-body login-card-body">
          
    
          <!-- /.social-auth-links -->
            <p class="login-box-msg" style="font-weight: 600;">Reset Password</p>
          {{-- <div class="card-header">{{ __('Reset Password') }}</div> --}}

                {{-- <div class="card-body"> --}}
                    <form method="POST" action="{{ route('password.update') }}">
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">

                        {{-- <div class="row mb-3">
                            <label for="email" class="col-md-4 col-form-label text-md-end">{{ __('Email Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="password" class="col-md-4 col-form-label text-md-end">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-end">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div> --}}

                        <div class="input-group mb-3">
                            {{-- <input type="email" class="form-control" placeholder="Email"> --}}
                            <input id="email" type="email" placeholder="Insert Your Email" class="form-control @error('email') is-invalid @enderror" value="{{ $email ?? old('email') }}" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                            <div class="input-group-append">
                              <div class="input-group-text">
                                <span class="fas fa-envelope"></span>
                              </div>
                            </div>
              
                                              @error('email')
                                                  <span class="invalid-feedback" role="alert">
                                                      <strong>{{ $message }}</strong>
                                                  </span>
                                              @enderror
                          </div>
                          <div class="input-group mb-3">
                            {{-- <input type="password" class="form-control" placeholder="Password"> --}}
                            <input id="password" type="password" placeholder="Insert Your Password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                            <div class="input-group-append">
                              <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                              </div>
                            </div>
              
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                          </div>
                          <div class="input-group mb-3">
                            {{-- <input type="password" class="form-control" placeholder="Password"> --}}
                            <input id="password-confirm" type="password" placeholder="Insert Password Confirmation" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            
                            <div class="input-group-append">
                              <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                              </div>
                            </div>
              
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                          </div>

                        {{-- <div class="row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-sm btn-primary">
                                    {{ __('Reset Password') }}
                                </button>
                            </div>
                        </div> --}}
                        <div class="row">
              
                            <!-- /.col -->
                            <div class="col-8">
                              <button type="submit" class="btn btn-sm btn-secondary btn-block">Reset Password</button>
                            </div>
                            <!-- /.col -->
                          </div>
                    </form>
                {{-- </div> --}}
          
          {{-- <p class="mb-0">
            <a href="register.html" class="text-center">Register </a>
          </p> --}}
        </div>
        <!-- /.login-card-body -->
      </div>
    </div>
    <!-- /.login-box -->
    
    <!-- jQuery -->
    <script src="{{asset('public/asset/')}}pluginsjquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="{{asset('public/asset/')}}pluginsbootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('public/asset/')}}dist/jsadminlte.min.js"></script>
    </body>
@endsection
