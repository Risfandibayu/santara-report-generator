@extends('auth.master')

@section('content')


<body class="hold-transition login-page" style="background-color: #191919;">
    <div class="login-box">
      <div class="login-logo">
        <img src="{{asset('public/asset')}}/dist/img/logo_header.png"><br>
        <a href="/login" style="color: white"><b>Santara</b> API Management</a>
      </div>
      <!-- /.login-logo -->
      <div class="card">
        <div class="card-body login-card-body">
          
    
          <!-- /.social-auth-links -->
          {{-- <div class="card-header">{{ __('Verify Your Email Address') }}</div> --}}
          <p class="login-box-msg" style="font-weight: 600;">Verify Your Email Address</p>

          <div class="card-body">
              @if (session('resent'))
                  <div class="alert alert-success" role="alert">
                      {{ __('A fresh verification link has been sent to your email address.') }}
                  </div>
              @endif

              {{ __('Before proceeding, please check your email for a verification link.') }}
              {{ __('If you did not receive the email') }},
              <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                  @csrf
                  <button type="submit" class="btn btn-link p-0 m-0 align-baseline">{{ __('click here to request another') }}</button>.
              </form>
          </div>
          
          {{-- <p class="mb-0">
            <a href="register.html" class="text-center">Register </a>
          </p> --}}
        </div>
        <!-- /.login-card-body -->
      </div>
    </div>
    <!-- /.login-box -->
    
    <!-- jQuery -->
    <script src="{{asset('public/asset/')}}pluginsjquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="{{asset('public/asset/')}}pluginsbootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('public/asset/')}}dist/jsadminlte.min.js"></script>
    </body>
@endsection
