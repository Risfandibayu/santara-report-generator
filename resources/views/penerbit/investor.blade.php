@extends('layouts.app')

@section('style')
<link rel="stylesheet" href="{{ asset('public/asset') }}/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet"
    href="{{ asset('public/asset') }}/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<link rel="stylesheet" href="{{ asset('public/asset') }}/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
@endsection
@section('js')
<script src="{{ asset('public/asset') }}/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{{ asset('public/asset') }}/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="{{ asset('public/asset') }}/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="{{ asset('public/asset') }}/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="{{ asset('public/asset') }}/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="{{ asset('public/asset') }}/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="{{ asset('public/asset') }}/plugins/jszip/jszip.min.js"></script>
<script src="{{ asset('public/asset') }}/plugins/pdfmake/pdfmake.min.js"></script>
<script src="{{ asset('public/asset') }}/plugins/pdfmake/vfs_fonts.js"></script>
<script src="{{ asset('public/asset') }}/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="{{ asset('public/asset') }}/plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="{{ asset('public/asset') }}/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>

<script src="{{ asset('public/asset') }}/plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="{{ asset('public/asset') }}/plugins/jquery-validation/additional-methods.min.js"></script>

<!-- AdminLTE App -->
{{-- <script src="{{asset('public/asset')}}/dist/js/adminlte.min.js"></script> --}}
<!-- AdminLTE for demo purposes -->
{{-- <script src="{{asset('public/asset')}}/dist/js/demo.js"></script> --}}
<!-- Page specific script -->
<script>
    $(function() {
            $("#example1").DataTable({
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,
                "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

            $("#example3").DataTable({
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,
                "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
            }).buttons().container().appendTo('#example3_wrapper .col-md-6:eq(0)');

            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true,
                "responsive": true,
            });
        });
</script>
@endsection

@section('content')
<?php
    function pecahtgll($timestamp)
    {
        $pecahkan = preg_split('/( |,|-|:)/', $timestamp);
        $hari = [
            7 => 'Minggu',
            1 => 'Senin',
            2 => 'Selasa',
            3 => 'Rabu',
            4 => 'Kamis',
            5 => 'Jumat',
            6 => 'Sabtu',
        ];
        $bulan = [
            1 => 'Januari',
            2 => 'Februari',
            3 => 'Maret',
            4 => 'April',
            5 => 'Mei',
            6 => ' Juni',
            7 => 'Juli',
            8 => 'Agustus',
            9 => 'September',
            10 => 'Oktober',
            11 => 'November',
            12 => 'Desember',
        ];
        return $hari[(int) $pecahkan[0]] . ',' . $pecahkan[1] . ' ' . $pecahkan[2] . ' ' . $pecahkan[4] . ' ' . $bulan[(int) $pecahkan[5]] . ' ' . $pecahkan[6] . ' ' . $pecahkan[7] . ' ' . $pecahkan[8] . ' ' . $pecahkan[1];
    }
    ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0" style="text-transform: uppercase; font-weight: 600;">Data Log</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Data</a></li>
                        <li class="breadcrumb-item active">Data Penerbit</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">


                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Data Log Penerbit


                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example3" class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        {{-- <th>ID</th> --}}
                                        {{-- <th>Time Log</th> --}}
                                        <th>Nama</th>
                                        <th>Email</th>
                                        <th>Nomor Telepon</th>
                                        <th>Saham</th>
                                        <th>Saldo Wallet</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $no = 0;
                                        ?>
                                    @foreach ($inv as $in)
                                    <?php
                                            $no++;
                                            ?>
                                    <tr>
                                        <td>{{ $no }}</td>
                                        {{-- <td>{{$ps->eid}}</td> --}}
                                        {{-- <td>{{$ps->timelog}}</td> --}}
                                        <td>{{ $in->nm }}</td>
                                        <td>{{ $in->em }}</td>
                                        <td>{{ $in->ph }}</td>
                                        <td>Rp {{ number_format($in->saham, 0, ',', '.') }}</td>
                                        <td>Rp {{ number_format($in->saldo, 0, ',', '.') }}</td>


                                    </tr>
                                    @endforeach

                                </tbody>

                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->


                </div>
                <!-- /.card -->


            </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->
</div>
<!-- /.row (main row) -->
</div><!-- /.container-fluid -->
</section>
<!-- /.content -->
</div>
@endsection