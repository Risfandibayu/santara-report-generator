@extends('layouts.app')

@section('style')
    <link rel="stylesheet" href="{{ asset('public/asset') }}/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet"
        href="{{ asset('public/asset') }}/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" href="{{ asset('public/asset') }}/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
@endsection
@section('js')
    <script src="{{ asset('public/asset') }}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{ asset('public/asset') }}/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{ asset('public/asset') }}/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
    <script src="{{ asset('public/asset') }}/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
    <script src="{{ asset('public/asset') }}/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
    <script src="{{ asset('public/asset') }}/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
    <script src="{{ asset('public/asset') }}/plugins/jszip/jszip.min.js"></script>
    <script src="{{ asset('public/asset') }}/plugins/pdfmake/pdfmake.min.js"></script>
    <script src="{{ asset('public/asset') }}/plugins/pdfmake/vfs_fonts.js"></script>
    <script src="{{ asset('public/asset') }}/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
    <script src="{{ asset('public/asset') }}/plugins/datatables-buttons/js/buttons.print.min.js"></script>
    <script src="{{ asset('public/asset') }}/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>

    <script src="{{ asset('public/asset') }}/plugins/jquery-validation/jquery.validate.min.js"></script>
    <script src="{{ asset('public/asset') }}/plugins/jquery-validation/additional-methods.min.js"></script>

    <!-- AdminLTE App -->
    {{-- <script src="{{asset('public/asset')}}/dist/js/adminlte.min.js"></script> --}}
    <!-- AdminLTE for demo purposes -->
    {{-- <script src="{{asset('public/asset')}}/dist/js/demo.js"></script> --}}
    <!-- Page specific script -->
    <script>
        $(function() {
            $("#example1").DataTable({
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,
                "columnDefs": [{
                    "targets": 8,
                    "visible": false
                }],
                "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

            $("#example3").DataTable({
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,

                "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
            }).buttons().container().appendTo('#example3_wrapper .col-md-6:eq(0)');

            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });
        });
    </script>
    @foreach ($penerbit_submited as $pse)
        <script>
            $(function() {
                $('#quickForm' + <?php echo $pse->id; ?>).validate({
                    rules: {
                        owner: {
                            required: true,
                        },
                        no_telp: {
                            required: true,
                        },
                        email: {
                            required: true,
                            email: true,
                        },
                        nama_pt: {
                            required: true,
                        },
                        nama_brand: {
                            required: true,
                        },
                        bidang_usaha: {
                            required: true,
                        },
                        total_pendanaan: {
                            required: true,
                            number: true
                        },
                        deskripsi: {
                            required: true,
                        },
                    },
                    messages: {
                        owner: {
                            required: "Please enter owner",
                        },
                        no_telp: {
                            required: "Please enter nomor telepon",
                        },
                        email: {
                            required: "Please enter a email address",
                            email: "Please enter a valid email address"
                        },
                        nama_pt: {
                            required: "Please enter nama perusahaan",
                        },
                        nama_brand: {
                            required: "Please enter nama brand",
                        },
                        bidang_usaha: {
                            required: "Please enter bidang usaha",
                        },
                        total_pendanaan: {
                            required: "Please enter total pendanaan",
                            number: "Please enter valid total pendanaan"
                        },
                        deskripsi: {
                            required: "Please enter deskripsi",
                        },

                    },
                    errorElement: 'span',
                    errorPlacement: function(error, element) {
                        error.addClass('invalid-feedback');
                        element.closest('.form-group').append(error);
                    },
                    highlight: function(element, errorClass, validClass) {
                        $(element).addClass('is-invalid');
                    },
                    unhighlight: function(element, errorClass, validClass) {
                        $(element).removeClass('is-invalid');
                    }
                });
            });
        </script>
    @endforeach
    <?php $no = 0; ?>
    @foreach ($penerbit as $pen)
        <?php $no++; ?>
        <script>
            $(function() {
                $('#quickForm' + <?php echo $no; ?>).validate({
                    rules: {
                        owner: {
                            required: true,
                        },
                        no_telp: {
                            required: true,
                        },
                        email: {
                            required: true,
                            email: true,
                        },
                        nama_pt: {
                            required: true,
                        },
                        nama_brand: {
                            required: true,
                        },
                        bidang_usaha: {
                            required: true,
                        },
                        total_pendanaan: {
                            required: true,
                            number: true
                        },
                        deskripsi: {
                            required: true,
                        },
                    },
                    messages: {
                        owner: {
                            required: "Please enter owner",
                        },
                        no_telp: {
                            required: "Please enter nomor telepon",
                        },
                        email: {
                            required: "Please enter a email address",
                            email: "Please enter a valid email address"
                        },
                        nama_pt: {
                            required: "Please enter nama perusahaan",
                        },
                        nama_brand: {
                            required: "Please enter nama brand",
                        },
                        bidang_usaha: {
                            required: "Please enter bidang usaha",
                        },
                        total_pendanaan: {
                            required: "Please enter total pendanaan",
                            number: "Please enter valid total pendanaan"
                        },
                        deskripsi: {
                            required: "Please enter deskripsi",
                        },

                    },
                    errorElement: 'span',
                    errorPlacement: function(error, element) {
                        error.addClass('invalid-feedback');
                        element.closest('.form-group').append(error);
                    },
                    highlight: function(element, errorClass, validClass) {
                        $(element).addClass('is-invalid');
                    },
                    unhighlight: function(element, errorClass, validClass) {
                        $(element).removeClass('is-invalid');
                    }
                });
            });
        </script>
    @endforeach
@endsection

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0" style="text-transform: uppercase; font-weight: 600;">API Data Penerbit
                        </h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Data</a></li>
                            <li class="breadcrumb-item active">Data Penerbit</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Data Penerbit </h3>
                                <!-- <button class="btn btn-primary" style="float: right;"> Filter</button> -->
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="example1" class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center">#</th>
                                            <!-- <th style="text-align: center">ID</th> -->
                                            <th style="text-align: center" class="noVis">Owner</th>
                                            <th style="text-align: center">Nomor Telepon</th>
                                            <th style="text-align: center">Email</th>
                                            <th style="text-align: center">Nama Perusahaan</th>
                                            <th style="text-align: center">Nama Brand</th>
                                            <th style="text-align: center">Bidang Usaha</th>
                                            <th style="text-align: center">Total Pendanaan</th>
                                            <th style="text-align: center">Deskripsi</th>
                                            <!-- <th style="text-align: center" >Status</th> -->
                                            <th style="text-align: center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $no = 0;
                                        ?>
                                        @foreach ($penerbit as $p)
                                            <?php
                                            $no++;
                                            ?>
                                            <tr>
                                                <td>{{ $no }}</td>
                                                {{-- <td>{{$p->eid}}</td> --}}
                                                <td>{{ $p->name }}</td>
                                                <td>{{ $p->phone }}</td>
                                                <td>{{ $p->email }}</td>
                                                <td>{{ $p->com }}</td>
                                                <td>{{ $p->tm }}</td>
                                                <td>{{ $p->cat }}</td>
                                                <td>Rp. {{ number_format(round($p->dana, 0), 2, ',', '.') }}</td>
                                                <td>{{ $p->des }}</td>
                                                <!-- <td>0</td> -->
                                                <td>
                                                    <!-- <form action="{{ url('/penerbit/store') }}" method="POST" enctype="multipart/form-data">
                                                    {{ csrf_field() }}
                                                    <input type="text" name="eid" hidden value="{{ $p->eid }}">
                                                    <input type="text" name="tid" hidden value="{{ $p->tid }}">
                                                    <input type="text" name="uid" hidden value="{{ $p->uid }}">
                                                    <input type="text" name="cid" hidden value="{{ $p->cid }}">
                                                    <input type="text" name="owner" hidden value="{{ $p->name }}">
                                                    <input type="text" name="no_telp" hidden value="{{ $p->phone }}">
                                                    <input type="text" name="email" hidden value="{{ $p->email }}">
                                                    <input type="text" name="nama_pt" hidden value="{{ $p->com }}">
                                                    <input type="text" name="nama_brand" hidden value="{{ $p->tm }}">
                                                    <input type="text" name="bidang_usaha" hidden value="{{ $p->cat }}">
                                                    <input type="text" name="total_pendanaan" hidden value="{{ round($p->dana, 0) }}">
                                                    <input type="text" name="deskripsi" hidden value="{{ $p->des }}">
                                                    <input type="text" name="status" hidden value="35">
                                                    <button type="submit" class="btn btn-primary">
                                                        Submit
                                                    </button> -->
                                                    </form>

                                                    <button type="button" class="btn btn-primary" data-toggle="modal"
                                                        data-target="#submit{{ $no }}">
                                                        Submit
                                                    </button>
                                                </td>

                                            </tr>
                                        @endforeach

                                    </tbody>

                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->


                    </div>
                    <!-- /.card -->
                </div>

                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Data Penerbit Submited</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example3" class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        {{-- <th>ID</th> --}}
                                        <th>Owner</th>
                                        <th>Nomor Telepon</th>
                                        <th>Email</th>
                                        <th>Nama Perusahaan</th>
                                        <th>Nama Brand</th>
                                        <th>Bidang Usaha</th>
                                        <th>Total Pendanaan</th>
                                        <th>Deskripsi</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $no = 0;
                                    ?>
                                    @foreach ($penerbit_submited as $ps)
                                        <?php
                                        $no++;
                                        ?>
                                        <tr>
                                            <td>{{ $no }}</td>
                                            {{-- <td>{{$ps->eid}}</td> --}}
                                            <td>{{ $ps->owner }}</td>
                                            <td>{{ $ps->no_telp }}</td>
                                            <td>{{ $ps->email }}</td>
                                            <td>{{ $ps->nama_pt }}</td>
                                            <td>{{ $ps->nama_brand }}</td>
                                            <td>{{ $ps->bidang_usaha }}</td>
                                            <td>Rp. {{ number_format(round($ps->total_pendanaan, 0), 2, ',', '.') }}</td>
                                            <td>{{ $ps->deskripsi }}</td>
                                            <td>
                                                {{ $ps->sta }}

                                            </td>
                                            <td>

                                                <button type="submit" class="btn btn-success" data-toggle="modal"
                                                    data-target="#update_status{{ $ps->id }}">
                                                    Update Status
                                                </button>
                                                <button type="button" class="btn btn-warning" data-toggle="modal"
                                                    data-target="#update{{ $ps->id }}">
                                                    Edit
                                                </button>
                                            </td>

                                        </tr>
                                    @endforeach

                                </tbody>

                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->


                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
    </div>
    <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    </div>
    
    <?php $no = 0; ?>
    @foreach ($penerbit as $pen)
        <?php $no++; ?>
        <div class="modal fade" id="submit{{ $no }}">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Edit Penerbit</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form id="quickForm{{ $no }}" action="{{ url('/penerbit/store') }}" method="POST"
                        enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="modal-body">
                            <div class="row">
                                <input type="text" name="eid" hidden value="{{ $pen->eid }}">
                                <input type="text" name="tid" hidden value="{{ $pen->tid }}">
                                <input type="text" name="uid" hidden value="{{ $pen->uid }}">
                                <input type="text" name="cid" hidden value="{{ $pen->cid }}">
                                <input type="text" name="status" hidden value="39">
                                <div class="form-group col-md-4">
                                    <label for="exampleInputEmail1">Owner</label>
                                    <input type="text" name="owner" class="form-control" value="{{ $pen->name }}"
                                        placeholder="Enter Owner">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="exampleInputEmail1">Nomor Telepon</label>
                                    <input type="text" name="no_telp" class="form-control" value="{{ $pen->phone }}"
                                        placeholder="Enter Nomor Telepon">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="exampleInputEmail1">Email address</label>
                                    <input type="email" name="email" class="form-control" value="{{ $pen->email }}"
                                        placeholder="Enter email">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Nama Perusahaan</label>
                                <input type="text" name="nama_pt" class="form-control" value="{{ $pen->com }}"
                                    placeholder="Enter Nama Perusahaan">
                            </div>
                            <div class="row">

                                <div class="form-group col-md-6">
                                    <label for="exampleInputEmail1">Nama Brand</label>
                                    <input type="text" name="nama_brand" class="form-control" value="{{ $pen->tm }}"
                                        placeholder="Enter Nama Brand">
                                </div>



                                <div class="form-group col-md-6">
                                    <label for="exampleInputEmail1">Bidang Usaha</label>
                                    <!-- <input type="text" name="bidang_usaha" class="form-control" value="{{ $pen->cat }}" placeholder="Enter Bidang Usaha"> -->
                                    <select class="form-control" name="bidang_usaha" id="bidang_usaha">
                                        <option value="" hidden>-- Pilih Kategori --</option>
                                        @foreach ($c as $ct)
                                            <option value="{{ $ct->category }}"
                                                {{ $pen->cat == $ct->category ? 'selected' : '' }}>{{ $ct->category }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Total Pendanaan</label>
                                <input type="number" name="total_pendanaan" class="form-control"
                                    value="{{ round($pen->dana, 0) }}" placeholder="Enter Total Pendanaan">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Deskripsi</label>
                                <!-- <input type="Text" name="deskripsi" class="form-control" value="{{ $pen->des }}" placeholder="Enter Deskripsi"> -->
                                <textarea name="deskripsi" class="form-control" id="" cols="10"
                                    rows="2">{{ $pen->des }}</textarea>
                            </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>




                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    @endforeach

    @foreach ($penerbit_submited as $pse)
        <div class="modal fade" id="update{{ $pse->id }}">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Edit Penerbit</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form id="quickForm{{ $pse->id }}" action="{{ url('/penerbit/update') }}/{{ $pse->id }}"
                        method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="modal-body">
                            <div class="row">

                                <div class="form-group col-md-4">
                                    <label for="exampleInputEmail1">Owner</label>
                                    <input type="text" name="owner" class="form-control" value="{{ $pse->owner }}"
                                        placeholder="Enter Owner">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="exampleInputEmail1">Nomor Telepon</label>
                                    <input type="text" name="no_telp" class="form-control" value="{{ $pse->no_telp }}"
                                        placeholder="Enter Nomor Telepon">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="exampleInputEmail1">Email address</label>
                                    <input type="email" name="email" class="form-control" value="{{ $pse->email }}"
                                        placeholder="Enter email">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Nama Perusahaan</label>
                                <input type="text" name="nama_pt" class="form-control" value="{{ $pse->nama_pt }}"
                                    placeholder="Enter Nama Perusahaan">
                            </div>
                            <div class="row">

                                <div class="form-group col-md-6">
                                    <label for="exampleInputEmail1">Nama Brand</label>
                                    <input type="text" name="nama_brand" class="form-control"
                                        value="{{ $pse->nama_brand }}" placeholder="Enter Nama Brand">
                                </div>



                                <div class="form-group col-md-6">
                                    <label for="exampleInputEmail1">Bidang Usaha</label>
                                    <!-- <input type="text" name="bidang_usaha" class="form-control" value="{{ $pse->bidang_usaha }}" placeholder="Enter Bidang Usaha"> -->
                                    <select class="form-control" name="bidang_usaha" id="bidang_usaha">
                                        <option value="" hidden>-- Pilih Kategori --</option>
                                        @foreach ($c as $ct)
                                            <option value="{{ $ct->category }}"
                                                {{ $pse->bidang_usaha == $ct->category ? 'selected' : '' }}>
                                                {{ $ct->category }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Total Pendanaan</label>
                                <input type="number" name="total_pendanaan" class="form-control"
                                    value="{{ $pse->total_pendanaan }}" placeholder="Enter Total Pendanaan">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Deskripsi</label>
                                <!-- <input type="Text" name="deskripsi" class="form-control" value="{{ $pse->deskripsi }}" placeholder="Enter Deskripsi"> -->
                                <textarea name="deskripsi" class="form-control" id="" cols="10"
                                    rows="2">{{ $pse->deskripsi }}</textarea>
                            </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>




                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    @endforeach

    @foreach ($penerbit_submited as $pse)
        <div class="modal fade" id="update_status{{ $pse->id }}">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Update Status</h4>
                        <br>

                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="{{ url('/penerbit/update_status') }}/{{ $pse->id }}" method="POST"
                        enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="modal-body">
                            <p>{{ $pse->nama_pt }}</p>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Status</label>
                                <!-- <input type="text" name="status" class="form-control" value="{{ $pse->bidang_usaha }}" placeholder="Enter Status"> -->
                                <select class="form-control" name="status">
                                    <option value="" hidden>-- Pilih Status--</option>
                                    @foreach ($s as $st)
                                        <option value="{{ $st->kode }}"
                                            {{ $pse->status == $st->kode ? 'selected' : '' }}>{{ $st->status }}</option>
                                    @endforeach
                                </select>
                            </div>

                        </div>
                        <!-- /.card-body -->
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>




                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    @endforeach
@endsection
