@extends('layouts.app')

@section('style')
<link rel="stylesheet" href="{{asset('public/asset')}}/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{asset('public/asset')}}/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<link rel="stylesheet" href="{{asset('public/asset')}}/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">

@endsection
@section('js')
<script src="{{asset('public/asset')}}/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{{asset('public/asset')}}/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="{{asset('public/asset')}}/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="{{asset('public/asset')}}/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="{{asset('public/asset')}}/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="{{asset('public/asset')}}/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="{{asset('public/asset')}}/plugins/jszip/jszip.min.js"></script>
<script src="{{asset('public/asset')}}/plugins/pdfmake/pdfmake.min.js"></script>
<script src="{{asset('public/asset')}}/plugins/pdfmake/vfs_fonts.js"></script>
<script src="{{asset('public/asset')}}/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="{{asset('public/asset')}}/plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="{{asset('public/asset')}}/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>

<script src="{{asset('public/asset')}}/plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="{{asset('public/asset')}}/plugins/jquery-validation/additional-methods.min.js"></script>

<!-- AdminLTE App -->
{{-- <script src="{{asset('public/asset')}}/dist/js/adminlte.min.js"></script> --}}
<!-- AdminLTE for demo purposes -->
{{-- <script src="{{asset('public/asset')}}/dist/js/demo.js"></script> --}}
<!-- Page specific script -->
@endsection

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0" style="text-transform: uppercase; font-weight: 600;">API Penyelenggara</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Data</a></li>
                        <li class="breadcrumb-item active">Penyelenggara</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Penyelenggara</h3>
                            <!-- <button class="btn btn-primary" style="float: right;"> Filter</button> -->
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-hover">
                                <tr>
                                    <td colspan="4" style="font-weight: bolder;text-align: center">Data Penyelenggara</td>
                                </tr>
                                <tr>
                                    <td>Nama Perusahaan</td>
                                    <td colspan="3">PT. Santara Daya Inspiratama</td>
                                </tr>
                                <tr>
                                    <td>Nama Brand</td>
                                    <td colspan="3">Santara</td>
                                </tr>
                                <tr>
                                    <td>Jumlah Penerbit</td>
                                    <td colspan="3">{{count($jumlah_penerbit)}}</td>
                                </tr>
                                <tr>
                                    <td>Jumlah Penerbit Funded</td>
                                    <td colspan="3">{{count($jumlah_penerbit_funded)}}</td>
                                </tr>
                                <tr>
                                    <td>Jumlah Investor</td>
                                    <td colspan="3">{{$jumlah_investor->total_pemodal_total}}</td>
                                </tr>
                                <tr>
                                    <td>Jumlah Investor Deviden</td>
                                    <td colspan="3">Rp. {{number_format(round($jumlah_investor_deviden->jid,0),2,",",".")}}</td>
                                </tr>
                                <tr>
                                    <td>Jumlah Dana</td>
                                    <td colspan="3">Rp. {{number_format(round($jumlah_dana->total_pendanaan,0),2,",",".")}}</td>
                                </tr>
                                <tr>
                                    <td>Jumlah Deviden</td>
                                    <td colspan="3">Rp. {{number_format(round($jumlah_deviden->d,0),2,",",".")}}</td>
                                </tr>
                                <tr>
                                    <td>Jumlah Deviden Penerima</td>
                                    <td colspan="3">Rp. {{number_format(round($jumlah_deviden_penerima->jid,0),2,",",".")}}</td>
                                </tr>
                                <tr>
                                    <td colspan="4" style="font-weight: bolder;text-align: center">Jenis Penerbit</td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bolder;">Jenis</td>
                                    <td colspan="3" style="font-weight: bolder;">Jumlah</td>
                                </tr>
                                <?php
                                    $no = 0;
                                    ?>
                                @foreach ($jenis as $j)
                                <?php
                                    $no++;
                                    ?>
                                <tr>
                                    @foreach ($jen as $je)
                                    {{-- <tr> --}}
                                        @if ($j->cid == $je->kode)
                                            {{-- <td>{{$je->kode_ald}}</td> --}}
                                            <td>{{$j->cat}}</td>
                                            <td colspan="3">{{$j->jcat}}</td>
                                        @endif
                                        
                                    {{-- </tr> --}}
                                    @endforeach
                                    {{-- <td>{{$j->cat}}</td>
                                    <td colspan="2">{{$j->jcat}}</td> --}}
                                </tr>
                                @endforeach

                                {{-- @foreach ($jen as $je)
                                <tr>
                                    <td>{{$je->kode}}</td>
                                    <td colspan="2">{{$je->kode_ald}}</td>
                                </tr>
                                @endforeach --}}
                                <tr>
                                    <td colspan="4" style="font-weight: bolder;text-align: center">Lokasi Pemodal</td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bolder;">ID Negara</td>
                                    <td style="font-weight: bolder;">Provinsi</td>
                                    <td colspan="2" style="font-weight: bolder;">Jumlah</td>
                                </tr>
                                @foreach ($lokasi_pemodal as $lp)
                                <tr>
                                    <td>ID</td>
                                    <td>{{$lp->prov}} </td>
                                    <td colspan="2">{{$lp->jum}}</td>
                                </tr>
                                @endforeach
                                <tr>
                                    <td colspan="4" style="font-weight: bolder;text-align: center">Lokasi Penerbit</td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bolder;">ID Negara</td>
                                    <td style="font-weight: bolder;">Provinsi</td>
                                    <td style="font-weight: bolder;">Kota</td>
                                    <td style="font-weight: bolder;">Jumlah</td>
                                </tr>
                                @foreach ($lokasi_penerbit as $lpe)
                                <tr>
                                    <td>ID</td>
                                    <td>{{$lpe->prov}}</td>
                                    <td>{{$lpe->reg}}</td>
                                    <td>{{$lpe->jum}}</td>
                                </tr>
                                @endforeach

                                

                            </table>
                            <form action="{{route('penyelenggara.store')}}" method="POST" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="text" name="nama_pt" hidden value="PT. Santara Daya Inspiratama">
                                <input type="text" name="nama_brand" hidden value="Santara">
                                <input type="text" name="jpnb" hidden value="{{count($jumlah_penerbit)}}">
                                <input type="text" name="jpnbf" hidden value="{{count($jumlah_penerbit_funded)}}">
                                <input type="text" name="jinv" hidden value="{{$jumlah_investor->total_pemodal_total}}">
                                <input type="text" name="jinvd" hidden value="{{round($jumlah_investor_deviden->jid,0)}}">
                                <input type="text" name="jdana" hidden value="{{round($jumlah_dana->total_pendanaan,0)}}">
                                <input type="text" name="jdev" hidden value="{{round($jumlah_deviden->d,0)}}">
                                <input type="text" name="jdevp" hidden value="{{round($jumlah_deviden_penerima->jid,0)}}">
                                <?php
                                $no = 0;
                                ?>
                                @foreach ($jenis as $j)
                                <?php
                                    $no++;
                                    ?>
                                    @foreach ($jen as $je)
                                        @if ($j->cid == $je->kode)
                                            <input type="text" name="jenis[]" hidden value="{{$je->kode_ald}}">
                                            <input type="text" name="jum[]" hidden value="{{$j->jcat}}">
                                        @endif
                                    @endforeach
                                @endforeach
                                <?php
                                $no = 0;
                                ?>
                                @foreach ($lokasi_pemodal as $lp)
                                <?php
                                    $no++;
                                    ?>
                                    @foreach ($lok as $item)
                                        @if (strtolower($lp->prov) == strtolower($item->prov))
                                        <input type="text" name="neg[]" hidden value="ID">
                                        <input type="text" name="prov[]" hidden value="{{$item->kode_ald}}">
                                        <input type="text" name="j[]" hidden value="{{$lp->jum}}">
                                        @endif
                                    @endforeach
                                @endforeach
                                <?php
                                $no = 0;
                                ?>
                                @foreach ($lokasi_penerbit as $lp)
                                <?php
                                    $no++;
                                    ?>
                                    @foreach ($lok as $l)
                                        @foreach ($lokkota as $k)
                                            @if ($lp->rid == $k->id)
                                                @if ($lp->pid == $l->kode)
                                                <input type="text" name="negp[]" hidden value="ID">
                                                <input type="text" name="provp[]" hidden value="{{$l->kode_ald}}">
                                                <input type="text" name="kotp[]" hidden value="{{$k->kode}}">
                                                <input type="text" name="jp[]" hidden value="{{$lp->jum}}">
                                                @endif
                                            @endif
                                        @endforeach
                                    @endforeach
                                @endforeach
                                <button type="submit" style="float:right; margin-top: 15px;" class="btn btn-primary">
                                    Submit
                                </button>
                            </form> 
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->


                </div>
                <!-- /.card -->
            </div>



            </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->
</div>
<!-- /.row (main row) -->
</div><!-- /.container-fluid -->
</section>
<!-- /.content -->
</div>


@endsection