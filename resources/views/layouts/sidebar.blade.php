
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{url('home')}}" class="brand-link">
      <img src="{{asset('public/asset')}}/dist/img/logo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8;padding: 6px;">
      <span class="brand-text font-weight-light">API Management</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      {{-- <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{asset('public/asset')}}/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="{{url('home')}}" class="d-block">{{Auth::user()->name}}</a>
        </div>
      </div> --}}

      <!-- SidebarSearch Form -->
      

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          {{-- <li class="nav-item menu-open">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="./index.html" class="nav-link active">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Dashboard v1</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="./index2.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Dashboard v2</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="./index3.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Dashboard v3</p>
                </a>
              </li>
            </ul>
          </li> --}}
          
          <li class="nav-header ">Home</li>
          <li class="nav-item">
            <a href="{{url('home')}}" class="nav-link {{ 'home' == request()->path() ? 'active' : '' }}">
              <i class="nav-icon fas fa-home"></i>
              <p>Dashboard</p>
            </a>
          </li>
          

          <li class="nav-header">Data</li>
          <li class="nav-item">
            <a href="{{url('penerbit')}}" class="nav-link {{ 'penerbit' == request()->path() ? 'active' : '' }}">
              <i class="nav-icon fas fa-file"></i>
              <p>Penerbit</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{url('penyelenggara')}}" class="nav-link {{ 'penyelenggara' == request()->path() ? 'active' : '' }}">
              <i class="nav-icon fas fa-file"></i>
              <p>Penyelenggara</p>
            </a>
          </li>

          <li class="nav-header">Log</li>
          <li class="nav-item">
            <a href="{{url('datalog')}}" class="nav-link {{ 'datalog' == request()->path() ? 'active' : '' }}">
              <i class="nav-icon fas fa-align-justify"></i>
              <p>Data Log</p>
            </a>
          </li>
          {{-- <li class="nav-item">
            <a href="https://adminlte.io/docs/3.1/" class="nav-link">
              <i class="nav-icon fas fa-clock"></i>
              <p>Scheduler</p>
            </a>
          </li> --}}
         
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
