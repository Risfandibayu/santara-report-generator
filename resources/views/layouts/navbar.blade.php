<nav class="main-header navbar navbar-expand navbar-white navbar-light">
  <!-- Left navbar links -->
  <ul class="navbar-nav">
    <li class="nav-item">
      <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
    </li>


  </ul>

  <!-- Right navbar links -->
  <ul class="navbar-nav ml-auto">
    <!-- Navbar Search -->
    {{-- <li class="nav-item">
        <a class="nav-link" data-widget="navbar-search" href="#" role="button">
          <i class="fas fa-search"></i>
        </a>
        <div class="navbar-search-block">
          <form class="form-inline">
            <div class="input-group input-group-sm">
              <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
              <div class="input-group-append">
                <button class="btn btn-navbar" type="submit">
                  <i class="fas fa-search"></i>
                </button>
                <button class="btn btn-navbar" type="button" data-widget="navbar-search">
                  <i class="fas fa-times"></i>
                </button>
              </div>
            </div>
          </form>
        </div>
      </li> --}}

    <!-- Messages Dropdown Menu -->
    <?php
    function pecahtgl($timestamp)
{
    $pecahkan = preg_split('/( |,|-|:)/',$timestamp);
    $hari = array(
        7 => 'Minggu',
        1 => 'Senin',
        2 =>'Selasa',
        3 => 'Rabu',
        4 => 'Kamis',
        5 => 'Jumat',
        6 => 'Sabtu',
    ); $bulan = array(
        1 =>   'Januari',
        2 => 'Februari',
        3 => 'Maret',
        4 => 'April',
        5 => 'Mei',
        6 =>' Juni',
        7 => 'Juli',
        8 => 'Agustus',
        9 => 'September',
        10 => 'Oktober',
        11 => 'November',
        12 => 'Desember'
    );
    return $hari[(int)$pecahkan[0]].','.$pecahkan[1] . ' ' . $pecahkan[2] . ' ' . $pecahkan[4]. ' ' . $bulan[(int)$pecahkan[5]]. ' ' . $pecahkan[6]. ' ' . $pecahkan[7]. ' ' . $pecahkan[8]. ' ' . $pecahkan[1];
};

    use App\Models\akun;
    $ak = akun::where('user_id',Auth::user()->id)->first();
    ?>

@if($ak)
    <li class="nav-item">
      <button class="nav-link btn">
        Token Kadaluarsa Pada {{ pecahtgl(date('N, d - m - Y', strtotime($ak->valid_until)))}}{{date('H:i:s', strtotime($ak->valid_until))}}
      </button>
    </li>
    <li class="nav-item" style="margin-right: 5px;">
      <button class="nav-link btn btn-default" data-toggle="modal" data-target="#set_akun">
        Setup Akun
      </button>
    </li>
    <li class="nav-item">
      <form  action="{{url('/refresh_token')}}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
        <button type="submit" class="nav-link btn btn-default">
          Refresh Token
        </button>
      </form>
    </li>
@else
<li class="nav-item" style="margin-right: 5px;">
  <button class="nav-link btn btn-default" data-toggle="modal" data-target="#set_akun">
    Setup Akun
  </button>
</li>
@endif
    <li class="nav-item">
      <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();
                      document.getElementById('logout-form').submit();">
        <i style="color: red" class="fas fa-power-off"></i>
      </a>

      <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
        @csrf
      </form>
    </li>

  </ul>
</nav>