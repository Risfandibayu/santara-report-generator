@extends('layouts.app')

@section('style')
<link rel="stylesheet" href="{{asset('public/asset')}}/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{asset('public/asset')}}/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<link rel="stylesheet" href="{{asset('public/asset')}}/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">

@endsection
@section('js')
<script src="{{asset('public/asset')}}/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{{asset('public/asset')}}/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="{{asset('public/asset')}}/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="{{asset('public/asset')}}/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="{{asset('public/asset')}}/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="{{asset('public/asset')}}/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="{{asset('public/asset')}}/plugins/jszip/jszip.min.js"></script>
<script src="{{asset('public/asset')}}/plugins/pdfmake/pdfmake.min.js"></script>
<script src="{{asset('public/asset')}}/plugins/pdfmake/vfs_fonts.js"></script>
<script src="{{asset('public/asset')}}/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="{{asset('public/asset')}}/plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="{{asset('public/asset')}}/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>

<script src="{{asset('public/asset')}}/plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="{{asset('public/asset')}}/plugins/jquery-validation/additional-methods.min.js"></script>

<!-- AdminLTE App -->
{{-- <script src="{{asset('public/asset')}}/dist/js/adminlte.min.js"></script> --}}
<!-- AdminLTE for demo purposes -->
{{-- <script src="{{asset('public/asset')}}/dist/js/demo.js"></script> --}}
<!-- Page specific script -->
<script>
    $(function() {
        $("#example1").DataTable({
            "responsive": true,
            "lengthChange": false,
            "autoWidth": false,
            "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
        }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

        $("#example3").DataTable({
            "responsive": true,
            "lengthChange": false,
            "autoWidth": false,
            "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
        }).buttons().container().appendTo('#example3_wrapper .col-md-6:eq(0)');

        $('#example2').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": false,
            "info": true,
            "autoWidth": true,
            "responsive": true,
        });
        $('#example9').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": false,
            "info": true,
            "autoWidth": true,
            "responsive": true,
        });
    });
</script>
@endsection

@section('content')

<?php
    function pecahtgll($timestamp)
{
    $pecahkan = preg_split('/( |,|-|:)/',$timestamp);
    $hari = array(
        7 => 'Minggu',
        1 => 'Senin',
        2 =>'Selasa',
        3 => 'Rabu',
        4 => 'Kamis',
        5 => 'Jumat',
        6 => 'Sabtu',
    ); $bulan = array(
        1 =>   'Januari',
        2 => 'Februari',
        3 => 'Maret',
        4 => 'April',
        5 => 'Mei',
        6 =>' Juni',
        7 => 'Juli',
        8 => 'Agustus',
        9 => 'September',
        10 => 'Oktober',
        11 => 'November',
        12 => 'Desember'
    );
    return $hari[(int)$pecahkan[0]].','.$pecahkan[1] . ' ' . $pecahkan[2] . ' ' . $pecahkan[4]. ' ' . $bulan[(int)$pecahkan[5]]. ' ' . $pecahkan[6]. ' ' . $pecahkan[7]. ' ' . $pecahkan[8]. ' ' . $pecahkan[1];
};
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0" style="text-transform: uppercase; font-weight: 600;">Data Log</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Data</a></li>
                        <li class="breadcrumb-item active">Data Log</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">

            <div class="col-6">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Data Log Penerbit</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example9" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    {{-- <th>#</th> --}}
                                    {{-- <th>ID</th> --}}
                                    <th>Date</th>
                                    {{-- <th>Time</th>
                                    <th>Owner</th>
                                    <th>Nomor Telepon</th>
                                    <th>Email</th>
                                    <th>Nama Perusahaan</th>
                                    <th>Nama Brand</th>
                                    <th>Bidang Usaha</th>
                                    <th>Total Pendanaan</th>
                                    <th>Deskripsi</th>
                                    <th>Status</th> --}}
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 0;
                                ?>
                                @foreach($pnblog as $ps)
                                <?php
                                $no++;
                                ?>
                                <tr>
                                    {{-- <td>{{$no}}</td> --}}
                                    {{-- <td>{{$ps->eid}}</td> --}}
                                    <td>{{ pecahtgll(date('N, d - m - Y', strtotime($ps->time)))}}</td>
                                    {{-- <td>2022-02-11 12:12:12</td>
                                    <td>{{$ps->owner}}</td>
                                    <td>{{$ps->no_telp}}</td>
                                    <td>{{$ps->email}}</td>
                                    <td>{{$ps->nama_pt}}</td>
                                    <td>{{$ps->nama_brand}}</td>
                                    <td>{{$ps->bidang_usaha}}</td>
                                    <td>Rp. {{number_format(round($ps->total_pendanaan,0),2,",",".")}}</td>
                                    <td>{{$ps->deskripsi}}</td>
                                    <td>
                                        {{$ps->sta}}
                                        
                                    </td> --}}
                                    <td style="text-align: center">
{{-- 
    
                                        <button type="submit" class="btn btn-success" data-toggle="modal" data-target="#update_status{{$ps->id}}">
                                            Update Status
                                        </button> --}}
                                        <form action="{{route('datalog_detail_tgl',$ps->time )}}" method="POST" enctype="multipart/form-data">
                                            {{ csrf_field() }}

                                            <button type="submit" class="btn btn-primary" >
                                                Lihat Detail
                                            </button>
                                        </form>
                                    </td>

                                </tr>
                                @endforeach

                            </tbody>

                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->


            </div>
            <div class="col-6">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Data Log Penyelenggara</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    {{-- <th>#</th> --}}
                                    {{-- <th>ID</th> --}}
                                    <th>Date</th>
                                    {{-- <th>Time</th>
                                    <th>Owner</th>
                                    <th>Nomor Telepon</th>
                                    <th>Email</th>
                                    <th>Nama Perusahaan</th>
                                    <th>Nama Brand</th>
                                    <th>Bidang Usaha</th>
                                    <th>Total Pendanaan</th>
                                    <th>Deskripsi</th>
                                    <th>Status</th> --}}
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 0;
                                ?>
                                @foreach($pnylog as $ps)
                                <?php
                                $no++;
                                ?>
                                <tr>
                                    {{-- <td>{{$no}}</td> --}}
                                    {{-- <td>{{$ps->eid}}</td> --}}
                                    <td>{{ pecahtgll(date('N, d - m - Y', strtotime($ps->time)))}}</td>
                                    {{-- <td>2022-02-11 12:12:12</td>
                                    <td>{{$ps->owner}}</td>
                                    <td>{{$ps->no_telp}}</td>
                                    <td>{{$ps->email}}</td>
                                    <td>{{$ps->nama_pt}}</td>
                                    <td>{{$ps->nama_brand}}</td>
                                    <td>{{$ps->bidang_usaha}}</td>
                                    <td>Rp. {{number_format(round($ps->total_pendanaan,0),2,",",".")}}</td>
                                    <td>{{$ps->deskripsi}}</td>
                                    <td>
                                        {{$ps->sta}}
                                        
                                    </td> --}}
                                    <td style="text-align: center">
{{-- 
    
                                        <button type="submit" class="btn btn-success" data-toggle="modal" data-target="#update_status{{$ps->id}}">
                                            Update Status
                                        </button> --}}
                                        <form action="{{route('datalog_detail_tglpny',$ps->time )}}" method="POST" enctype="multipart/form-data">
                                            {{ csrf_field() }}

                                            <button type="submit" class="btn btn-primary" >
                                                Lihat Detail
                                            </button>
                                        </form>
                                    </td>

                                </tr>
                                @endforeach

                            </tbody>

                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->


            </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->
</div>
<!-- /.row (main row) -->
</div><!-- /.container-fluid -->
</section>
<!-- /.content -->
</div>



@endsection