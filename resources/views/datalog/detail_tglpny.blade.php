@extends('layouts.app')

@section('style')
<link rel="stylesheet" href="{{asset('public/asset')}}/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{asset('public/asset')}}/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<link rel="stylesheet" href="{{asset('public/asset')}}/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">

@endsection
@section('js')
<script src="{{asset('public/asset')}}/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{{asset('public/asset')}}/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="{{asset('public/asset')}}/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="{{asset('public/asset')}}/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="{{asset('public/asset')}}/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="{{asset('public/asset')}}/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="{{asset('public/asset')}}/plugins/jszip/jszip.min.js"></script>
<script src="{{asset('public/asset')}}/plugins/pdfmake/pdfmake.min.js"></script>
<script src="{{asset('public/asset')}}/plugins/pdfmake/vfs_fonts.js"></script>
<script src="{{asset('public/asset')}}/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="{{asset('public/asset')}}/plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="{{asset('public/asset')}}/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>

<script src="{{asset('public/asset')}}/plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="{{asset('public/asset')}}/plugins/jquery-validation/additional-methods.min.js"></script>

<!-- AdminLTE App -->
{{-- <script src="{{asset('public/asset')}}/dist/js/adminlte.min.js"></script> --}}
<!-- AdminLTE for demo purposes -->
{{-- <script src="{{asset('public/asset')}}/dist/js/demo.js"></script> --}}
<!-- Page specific script -->
<script>
    $(function () {
        $("#example1").DataTable({
            "responsive": true,
            "lengthChange": false,
            "autoWidth": false,
            "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
        }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

        $("#example3").DataTable({
            "responsive": true,
            "lengthChange": false,
            "autoWidth": false,
            "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
        }).buttons().container().appendTo('#example3_wrapper .col-md-6:eq(0)');

        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true,
            "responsive": true,
        });
    });

</script>
@endsection

@section('content')

<?php
    function pecahtgll($timestamp)
{
    $pecahkan = preg_split('/( |,|-|:)/',$timestamp);
    $hari = array(
        7 => 'Minggu',
        1 => 'Senin',
        2 =>'Selasa',
        3 => 'Rabu',
        4 => 'Kamis',
        5 => 'Jumat',
        6 => 'Sabtu',
    ); $bulan = array(
        1 =>   'Januari',
        2 => 'Februari',
        3 => 'Maret',
        4 => 'April',
        5 => 'Mei',
        6 =>' Juni',
        7 => 'Juli',
        8 => 'Agustus',
        9 => 'September',
        10 => 'Oktober',
        11 => 'November',
        12 => 'Desember'
    );
    return $hari[(int)$pecahkan[0]].','.$pecahkan[1] . ' ' . $pecahkan[2] . ' ' . $pecahkan[4]. ' ' . $bulan[(int)$pecahkan[5]]. ' ' . $pecahkan[6]. ' ' . $pecahkan[7]. ' ' . $pecahkan[8]. ' ' . $pecahkan[1];
};
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0" style="text-transform: uppercase; font-weight: 600;">Data Log</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Data</a></li>
                        <li class="breadcrumb-item active">Data Penerbit</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">


                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Data Log Penyelenggara
                                @foreach (array_slice($pny->toArray(), 0, 1) as $post)
                                {{ pecahtgll(date('N, d - m - Y', strtotime($post['time'])))}}
                                @endforeach
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example3" class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        {{-- <th>ID</th> --}}
                                        {{-- <th>Time Log</th> --}}
                                        <th>Keterangan</th>
                                        <th>Waktu Submit</th>

                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $no = 0;
                                    ?>
                                    @foreach($pny as $ps)
                                    <?php
                                    $no++;
                                    ?>
                                    <tr>
                                        <td>{{$no}}</td>
                                        {{-- <td>{{$ps->eid}}</td> --}}
                                        {{-- <td>{{$ps->timelog}}</td> --}}
                                        <td>PUSH DATA PENYELENGGARA</td>
                                        <td>{{$ps->time}}</td>
                                        <td>
                                            <button type="button" data-toggle="modal" data-target="#detail{{$ps->id}}"
                                                class="btn btn-primary">
                                                Lihat Detail
                                            </button>
                                        </td>

                                    </tr>
                                    @endforeach

                                </tbody>

                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->


                </div>
                <!-- /.card -->


            </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->
</div>
<!-- /.row (main row) -->
</div><!-- /.container-fluid -->
</section>
<!-- /.content -->
</div>



<?php
$no = 0;
?>
@foreach($pny as $ps)
<?php
$no++;
?>
@php
$value = \App\Models\jenis::where('jid',$ps->id)->select('jenis_sbt.*','j.jenis as name')
->join('jenis as j','j.kode_ald','=','jenis_sbt.jenis')
->get();
$prov = \App\Models\prov_sbt::where('pid',$ps->id)->select('prov_sbts.*','prov.prov as name')
->join('prov','prov.kode_ald','=','prov_sbts.provid')
->get();
$provpnb = \App\Models\provp_sbt::where('pid',$ps->id)->select('provp_sbts.*','prov.prov as name','kotas.kota as kname')
->join('prov','prov.kode_ald','=','provp_sbts.provid')
->join('kotas','kotas.kode','=','provp_sbts.kotid')
->get();

@endphp
<div class="modal fade" id="detail{{$ps->id}}">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Detail Log Data Penyelenggara</h4>
                <br>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <table id="example8" class="table table-bordered table-hover">
                    <tr>
                        <td colspan="3" style="font-weight: bolder;text-align: center">Data Penyelenggara</td>
                    </tr>
                    <tr>
                        <td>Nama Perusahaan</td>
                        <td colspan="3">{{$ps->nama_pt}}</td>
                    </tr>
                    <tr>
                        <td>Nama Brand</td>
                        <td colspan="3">{{$ps->nama_brand}}</td>
                    </tr>
                    <tr>
                        <td>Jumlah Penerbit</td>
                        <td colspan="3">{{$ps->jumlah_penerbit}}</td>
                    </tr>
                    <tr>
                        <td>Jumlah Penerbit Funded</td>
                        <td colspan="3">{{$ps->jumlah_penerbit_funded}}</td>
                    </tr>
                    <tr>
                        <td>Jumlah Investor</td>
                        <td colspan="3">{{$ps->jumlah_investor}}</td>
                    </tr>
                    <tr>
                        <td>Jumlah Investor Deviden</td>
                        <td colspan="3">Rp. {{number_format(round($ps->jumlah_investor_dividen,0),2,",",".")}}</td>
                    </tr>
                    <tr>
                        <td>Jumlah Dana</td>
                        <td colspan="3">Rp. {{number_format(round($ps->jumlah_dana,0),2,",",".")}}</td>
                    </tr>
                    <tr>
                        <td>Jumlah Deviden</td>
                        <td colspan="3">Rp. {{number_format(round($ps->jumlah_dividen,0),2,",",".")}}</td>
                    </tr>
                    <tr>
                        <td>Jumlah Deviden Penerima</td>
                        <td colspan="3">Rp. {{number_format(round($ps->jumlah_dividen_penerima,0),2,",",".")}}</td>
                    </tr>
                    <tr>
                        <td colspan="4" style="font-weight: bolder;text-align: center">Jenis Penerbit</td>
                    </tr>
                    <tr>
                        <td style="font-weight: bolder;">Jenis</td>
                        <td colspan="3" style="font-weight: bolder;">Jumlah</td>
                    </tr>
                    @foreach ($value as $j)
                    <tr>
                        <td>{{$j->name}}</td>
                        <td colspan="3">{{$j->jumlah}}</td>
                    </tr>
                    @endforeach
                    <tr>
                        <td colspan="4" style="font-weight: bolder;text-align: center">Lokasi Pemodal</td>
                    </tr>
                    <tr>
                        <td style="font-weight: bolder;">ID Negara</td>
                        <td style="font-weight: bolder;">Provinsi</td>
                        <td colspan="2" style="font-weight: bolder;">Jumlah</td>
                    </tr>
                    @foreach ($prov as $j)
                    <tr>
                        <td>{{$j->negid}}</td>
                        <td>{{$j->name}}</td>
                        <td colspan="3">{{$j->jumlah}}</td>
                    </tr>
                    @endforeach
                    <tr>
                        <td colspan="4" style="font-weight: bolder;text-align: center">Lokasi Penerbit</td>
                    </tr>
                    <tr>
                        <td style="font-weight: bolder;">ID Negara</td>
                        <td style="font-weight: bolder;">Provinsi</td>
                        <td style="font-weight: bolder;">Kota</td>
                        <td style="font-weight: bolder;">Jumlah</td>
                    </tr>
                    @foreach ($provpnb as $j)
                    <tr>
                        <td>{{$j->negid}}</td>
                        <td>{{$j->name}}</td>
                        <td>{{$j->kname}}</td>
                        <td colspan="3">{{$j->jumlah}}</td>
                    </tr>
                    @endforeach

                </table>
            </div>
            <!-- /.card-body -->
            {{-- <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
            </div> --}}




        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endforeach
@endsection
